#include <chrono>
#include <iostream>

#include "util/thread_pool.hpp"


void fake_work() {
    std::this_thread::sleep_for(std::chrono::seconds(5));
}


void test1() {
    rdftrie::ThreadPool th(2);
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    std::cout << "Submitted jobs" << std::endl;
}

void test2() {
    rdftrie::ThreadPool th(4);
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    std::cout << "Submitted jobs" << std::endl;
    th.wait_completion();
    std::cout << "Jobs completed !" << std::endl;
}

void test3() {
    rdftrie::ThreadPool th(4);
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    std::cout << "Submitted jobs batch 1" << std::endl;
    th.wait_completion();
    std::cout << "Batch 1 completed !" << std::endl;
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    th.submit_job(std::bind(fake_work));
    std::cout << "Submitted jobs batch 2" << std::endl;
    th.wait_completion();
    std::cout << "Batch 2 completed !" << std::endl;
}

int main(int argc, char const *argv[])
{
    // test1();
    // std::cout << "Done test 1" << std::endl;
    // test2();
    test3();
    return 0;
}
