#include <iostream>
#include <cassert>

#include "storage/dictionary/trie_dictionary.hpp"
#include "rdf/term/rdf_term.hpp"


void test1() {
    rdftrie::MemTrieDictionary trie;
    std::string uri = "test.org/abc";
    rdftrie::Term term(uri, rdftrie::URI, rdftrie::PREDICATE);
    trie.string_to_id(term);
    std::cout << "Test 1" << std::endl;
}


void test2() {
    rdftrie::MemTrieDictionary trie;
    std::string uri = "test.org/abc";
    rdftrie::Term term(uri, rdftrie::URI, rdftrie::PREDICATE);
    uintptr_t id = trie.string_to_id(term);
    uintptr_t id2 = trie.string_to_id(term);
    assert(id == id2);
    std::cout << "Test 2" << std::endl;
}


void test3() {
    rdftrie::MemTrieDictionary trie;
    std::string uri1 = "test.org/abc";
    std::string uri2 = "test.org/def";
    std::string uri3 = "dbpedia.org/abc";
    rdftrie::Term term1(uri1, rdftrie::URI, rdftrie::PREDICATE);
    rdftrie::Term term2(uri2, rdftrie::URI, rdftrie::PREDICATE);
    rdftrie::Term term3(uri3, rdftrie::URI, rdftrie::PREDICATE);
    uintptr_t id1 = trie.string_to_id(term1);
    uintptr_t id2 = trie.string_to_id(term2);
    uintptr_t id3 = trie.string_to_id(term3);
    assert(id1 != id2);
    assert(id2 != id3);
    assert(id1 != id3);
    std::cout << "Test 3" << std::endl;
}


void test4() {
    rdftrie::MemTrieDictionary trie;
    std::string lit = "abcdefghijkl";
    std::string uri = "test.org/abc";
    rdftrie::Term term1(uri, rdftrie::URI, rdftrie::PREDICATE);
    rdftrie::Term term2(lit, rdftrie::LITERAL, rdftrie::OBJECT);
    trie.string_to_id(term1);
    trie.string_to_id(term2);
    std::cout << "Test 4" << std::endl;
}


void test5() {
    rdftrie::MemTrieDictionary trie;
    std::string uri = "test.org/abc/def/ghi";
    rdftrie::Term term1(uri, rdftrie::URI, rdftrie::PREDICATE);
    uintptr_t id = trie.string_to_id(term1);
    std::string rid = trie.id_to_string(id);
    assert(uri == rid);
    std::cout << "Test 5" << std::endl;
}


void test6() {
    rdftrie::MemTrieDictionary trie;
    std::string lit = "abcdefghijkl";
    rdftrie::Term term1(lit, rdftrie::LITERAL, rdftrie::OBJECT);
    uintptr_t id = trie.string_to_id(term1);
    std::string rid = trie.id_to_string(id);
    assert(lit == rid);
    std::cout << "Test 6" << std::endl;
}


void test7() {
    rdftrie::MemTrieDictionary trie;
    std::string lit = "abcdefghijkl";
    rdftrie::Term term1(lit, rdftrie::UNKNOWN, rdftrie::QUERY);
    uintptr_t id = trie.string_to_id(term1);
    assert(id == 0UL);
    std::cout << "Test 7" << std::endl;
}


void test8() {
    rdftrie::MemTrieDictionary trie;
    std::string uri = "test.org/abc/def/ghi";
    rdftrie::Term term1(uri, rdftrie::UNKNOWN, rdftrie::QUERY);
    uintptr_t id = trie.string_to_id(term1);
    assert(id == 0UL);
    std::cout << "Test 8" << std::endl;
}


void test9() {
    rdftrie::MemTrieDictionary trie;
    std::string uri = "test.org/abc/def/ghi";
    rdftrie::Term term1(uri, rdftrie::URI, rdftrie::PREDICATE);
    uintptr_t id = trie.string_to_id(term1);
    rdftrie::Term term2(uri, rdftrie::UNKNOWN, rdftrie::QUERY);
    uintptr_t id2 = trie.string_to_id(term2);
    assert(id == id2);
    std::cout << "Test 9" << std::endl;
}


void test10() {
    rdftrie::MemTrieDictionary trie;
    std::string lit = "abcdefghijkl";
    rdftrie::Term term1(lit, rdftrie::LITERAL, rdftrie::OBJECT);
    uintptr_t id = trie.string_to_id(term1);
    rdftrie::Term term2(lit, rdftrie::UNKNOWN, rdftrie::QUERY);
    uintptr_t id2 = trie.string_to_id(term2);
    assert(id == id2);
    std::cout << "Test 10" << std::endl;
}


void test11() {
    rdftrie::MemTrieDictionary trie;
    std::string uri = "test.org/abc/def/ghi";
    rdftrie::Term term1(uri, rdftrie::URI, rdftrie::PREDICATE);
    uintptr_t id = trie.string_to_id(term1);
    rdftrie::Term term2(uri, rdftrie::URI, rdftrie::PREDICATE);
    uintptr_t id2 = trie.string_to_id(term2);
    assert(id == id2);
    std::cout << "Test 11" << std::endl;
}


int main(int argc, char const *argv[])
{
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
    test10();
    test11();
    return 0;
}
