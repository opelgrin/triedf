#include <iostream>
#include <cassert>

#include "storage/stores/trees/mem_children.hpp"


void test1() {
    rdftrie::MemChildren<unsigned> mc;
    assert(true);
}


void test2() {
    rdftrie::MemChildren<std::string> mc;
    std::string bd("test");
    uint64_t val = 12;
    mc.set(bd, val);
    assert(true);
}


void test3() {
    rdftrie::MemChildren<std::string> mc;
    std::string bd("test");
    uint64_t val = 12;
    mc.set(bd, val);
    uint64_t ret = mc.get(bd);
    assert(val == ret);
}


void test4() {
    rdftrie::MemChildren<std::string> mc;
    std::string bd("test");
    std::string bd2("test2");
    uint64_t val = 12;
    uint64_t val2 = 55;
    mc.set(bd, val);
    mc.set(bd2, val2);
    uint64_t ret = mc.get(bd);
    uint64_t ret2 = mc.get(bd2);
    assert(val == ret);
    assert(val2 == ret2);
}


int main(int argc, char const *argv[])
{
    test1();
    std::cout << "Test 1" << std::endl;
    test2();
    std::cout << "Test 2" << std::endl;
    test3();
    std::cout << "Test 3" << std::endl;
    test4();
    std::cout << "Test 4" << std::endl;
    return 0;
}
