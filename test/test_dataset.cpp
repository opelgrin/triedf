#include <iostream>
#include "rdf/dataset.hpp"

void print_vec(std::vector<std::string>& vec) {
    for (auto v: vec) {
        std::cout << v << " ";
    }
    std::cout << std::endl;
}

void print_ret(std::set<std::vector<std::string>>& ret) {
    for(auto v: ret) {
        print_vec(v);
    }
}

void test1() {
    std::vector<rdftrie::tuple_order> orders = {rdftrie::PSO};
    rdftrie::MemTripleDataset triple_dataset(orders);
    std::cout << "Test 1" << std::endl;
}

void test2() {
    std::vector<rdftrie::tuple_order> orders = {rdftrie::PSO};
    rdftrie::MemTripleDataset triple_dataset(orders);
    rdftrie::Term s("<test.org/s1>", rdftrie::URI, rdftrie::SUBJECT);
    rdftrie::Term p("<test.org/p1>", rdftrie::URI, rdftrie::PREDICATE);
    rdftrie::Term o("<test.org/o1>", rdftrie::URI, rdftrie::OBJECT);
    std::vector<rdftrie::Term> triple = {s, p, o};
    triple_dataset.add_tuple(triple);
    std::cout << "Test 2" << std::endl;
}

void test3() {
    std::vector<rdftrie::tuple_order> orders = {rdftrie::PSO};
    rdftrie::MemTripleDataset triple_dataset(orders);
    rdftrie::Term s("<test.org/s1>", rdftrie::URI, rdftrie::SUBJECT);
    rdftrie::Term p("<test.org/p1>", rdftrie::URI, rdftrie::PREDICATE);
    rdftrie::Term o("<test.org/o1>", rdftrie::URI, rdftrie::OBJECT);
    std::vector<rdftrie::Term> triple = {s, p, o};
    rdftrie::Term p2("<test.org/p2>", rdftrie::URI, rdftrie::PREDICATE);
    std::vector<rdftrie::Term> triple2 = {s, p2, o};
    triple_dataset.add_tuple(triple);
    triple_dataset.add_tuple(triple2);
    std::cout << "Test 3" << std::endl;
}

void test4() {
    std::vector<rdftrie::tuple_order> orders = {rdftrie::PSO};
    rdftrie::MemTripleDataset triple_dataset(orders);
    rdftrie::Term s("<test.org/s1>", rdftrie::URI, rdftrie::SUBJECT);
    rdftrie::Term p("<test.org/p1>", rdftrie::URI, rdftrie::PREDICATE);
    rdftrie::Term o("<test.org/o1>", rdftrie::URI, rdftrie::OBJECT);
    std::vector<rdftrie::Term> triple = {s, p, o};
    rdftrie::Term p2("<test.org/p2>", rdftrie::URI, rdftrie::PREDICATE);
    std::vector<rdftrie::Term> triple2 = {s, p2, o};
    triple_dataset.add_tuple(triple);
    triple_dataset.add_tuple(triple2);

    std::vector<std::string> p_string = {"?", "?", "?"};
    rdftrie::Pattern pattern(p_string, rdftrie::SPO);
    std::set<std::vector<std::string>> results;
    triple_dataset.query(pattern, results);
    
    print_ret(results);
    std::cout << "Test 4" << std::endl;
}

int main(int argc, char const *argv[])
{
    test1();
    test2();
    test3();
    test4();
    return 0;
}


