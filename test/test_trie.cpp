#include <iostream>
#include <cassert>

#include "storage/dictionary/mem_dict_trie.hpp"

void test1() {
    // rdftrie::MemDictTrie trie;
    rdftrie::MemDictTrie2 trie;
    std::string uri = "test.org/abc";
    trie.string_to_id(uri, 1);
    std::cout << "Test 1" << std::endl;
}

void test2() {
    // rdftrie::MemDictTrie trie;
    rdftrie::MemDictTrie2 trie;
    std::string uri = "test.org/abc";
    uint64_t id = trie.string_to_id(uri, 1);
    uint64_t id2 = trie.string_to_id(uri, 2);
    assert(id == id2);
    std::cout << "Test 2" << std::endl;
}

void test3() {
    // rdftrie::MemDictTrie trie;
    rdftrie::MemDictTrie2 trie;
    std::string uri1 = "test.org/abc";
    std::string uri2 = "test.org/def";
    std::string uri3 = "dbpedia.org/abc";
    uint64_t id1 = trie.string_to_id(uri1, 1);
    uint64_t id2 = trie.string_to_id(uri2, 2);
    uint64_t id3 = trie.string_to_id(uri3, 3);
    assert(id1 != id2);
    assert(id2 != id3);
    assert(id1 != id3);
    std::cout << "Test 3" << std::endl;
}

void test4() {
    // rdftrie::MemDictTrie trie;
    rdftrie::MemDictTrie2 trie;
    std::string uri = "test.org/abc/def/ghi";
    uint64_t id = trie.string_to_id(uri, 1);
    std::string rid = trie.id_to_string(id);
    assert(uri == rid);
    std::cout << "Test 4" << std::endl;
}

void test5() {
    // rdftrie::MemDictTrie trie;
    rdftrie::MemDictTrie2 trie;
    std::string uri = "test.org/abc/def/ghi";
    std::string uri2 = "test.org/123/456";
    uint64_t id = trie.string_to_id(uri, 1);
    std::string rid = trie.id_to_string(id);
    uint64_t id2 = trie.string_to_id(uri2, 2);
    std::string rid2 = trie.id_to_string(id2);
    assert(uri == rid);
    assert(uri2 == rid2);
    std::cout << "Test 5" << std::endl;
}

int main(int argc, char const *argv[])
{
    test1();
    test2();
    test3();
    test4();
    test5();
    return 0;
}
