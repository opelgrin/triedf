#include <iostream>

#include "rdf/parser/ntparser.hpp"
#include "storage/dictionary/mem_dictionary.hpp"
#include "storage/stores/mem_rdf_tree.hpp"

class LoadSink: public rdftrie::TripleSink {

    private:
        uint64_t __lines;
        rdftrie::MemDictionary __dict;
        rdftrie::MemRDFTree __spo;

    public:
        LoadSink(rdftrie::MemDictionary& dict, rdftrie::MemRDFTree& spo) {
            this->__dict = dict;
            this->__spo = __spo;
        }

        void triple(rdftrie::Term s, rdftrie::Term p, rdftrie::Term o) {
            uint64_t sid = this->__dict.string_to_id(s);
            uint64_t pid = this->__dict.string_to_id(p);
            uint64_t oid = this->__dict.string_to_id(o);
            std::vector<uint64_t> tuple = {sid, pid, oid};
            this->__spo.put(tuple);
            this->__lines++;
            if (this->__lines % 1 == 0) {
                std::cout << "Processed " << this->__lines << " triples..." << std::endl;
            }
        }

        uint64_t nlines() {
            return this->__lines;
        }

};


void load(std::string filename) {
    rdftrie::MemDictionary dict;
    rdftrie::MemRDFTree spo;
    LoadSink sink(dict, spo);
    rdftrie::NTParser p(&sink);
    p.parse(filename);
    std::cout << "Total processed lines: " << sink.nlines() << std::endl;
    std::cout << "Tuples in SPO Tree: " << spo.count_tuples() << std::endl;
}


int main(int argc, char const *argv[])
{
    // load(argv[1]);
    if (argc != 2) {
        std::cout << "Missing argument: <filename>" << std::endl;
        return 0;
    }
    load(argv[1]);
    return 0;
}
