#include <iostream>
#include <cassert>
#include <unordered_map>

#include "storage/bin_data.hpp"


void test1() {
    auto bd1 = rdftrie::BinData(std::string("test 1"));
    auto bd2 = rdftrie::BinData(std::string("test 2"));
    assert(bd1 != bd2);
}

void test2() {
    auto bd1 = rdftrie::BinData(std::string("test 1"));
    auto bd2 = rdftrie::BinData(std::string("test 1"));
    assert(bd1 == bd2);
}

void test3() {
    std::vector<char> v1 = {'a', 'c', 'v'};
    std::vector<char> v2 = {'t', 'e', 's', 't'};
    auto bd1 = rdftrie::BinData(v1);
    auto bd2 = rdftrie::BinData(v2);
    assert(bd1 != bd2);
}

void test4() {
    std::vector<char> v1 = {'a', 'c', 'v'};
    auto bd1 = rdftrie::BinData(v1);
    auto bd2 = rdftrie::BinData(v1);
    assert(bd1 == bd2);
}

void test5() { // test for char* constructor
}

void test6() { // test for unsigned constructor
}

void test7() { // test for uint64_t constructor
}

void test8() {
    std::unordered_map<rdftrie::BinData, unsigned> map;
    auto bd1 = rdftrie::BinData(std::string("test 1"));
    auto bd2 = rdftrie::BinData(std::string("test 2"));
    map.insert(std::make_pair(bd1, 1));
    map.insert(std::make_pair(bd2, 2));
}

int main(int argc, char const *argv[])
{
    test1();
    std::cout << "Test 1" << std::endl;
    test2();
    std::cout << "Test 2" << std::endl;
    test3();
    std::cout << "Test 3" << std::endl;
    test4();
    std::cout << "Test 4" << std::endl;
    test5();
    std::cout << "Test 5" << std::endl;
    test6();
    std::cout << "Test 6" << std::endl;
    test7();
    std::cout << "Test 7" << std::endl;
    test8();
    std::cout << "Test 8" << std::endl;
    return 0;
}
