#include <iostream>
#include <cassert>

// #include "storage/stores/mem_rdf_tree.hpp"
#include "storage/stores/mem_rdf_tree2.hpp"

void print_vec(std::vector<uint64_t>& vec) {
    for (auto v: vec) {
        std::cout << v << " ";
    }
    std::cout << std::endl;
}

void print_ret(std::set<std::vector<uint64_t>>& ret) {
    for(auto v: ret) {
        print_vec(v);
    }
}

void test1() {
    // rdftrie::MemRDFTree tree;
    rdftrie::MemRDFTree2 tree;
    assert(true);
    std::cout << "Test 1" << std::endl;
}


void test2() {
    // rdftrie::MemRDFTree tree;
    rdftrie::MemRDFTree2 tree;
    std::vector<uint64_t> t1 = {1, 2, 3};
    tree.put(t1);
    assert(true);
    std::cout << "Test 2" << std::endl;
}


void test3() {
    // rdftrie::MemRDFTree tree;
    rdftrie::MemRDFTree2 tree;
    std::vector<uint64_t> t1 = {1, 2, 3};
    std::vector<uint64_t> t2 = {5, 2, 9};
    tree.put(t1);
    tree.put(t2);
    assert(true);
    std::cout << "Test 3" << std::endl;
}


void test4() {
    // rdftrie::MemRDFTree tree;
    rdftrie::MemRDFTree2 tree;
    std::vector<uint64_t> t1 = {1, 2, 3};
    std::vector<uint64_t> t2 = {1, 2, 9};
    std::vector<uint64_t> t3 = {1, 2, 22};
    tree.put(t1);
    tree.put(t2);
    tree.put(t3);
    std::vector<uint64_t> p1 = {1};
    std::set<std::vector<uint64_t>> ret;
    tree.get(p1, ret);
    print_ret(ret);
    assert(true);
    std::cout << tree.count_tuples() << std::endl;
    std::cout << "Test 4" << std::endl;
}

void test5() {
    // rdftrie::MemRDFTree tree;
    rdftrie::MemRDFTree2 tree;
    std::vector<uint64_t> t1 = {1UL, 2UL, 3UL};
    std::vector<uint64_t> t2 = {1UL, 2UL, 4UL};
    std::vector<uint64_t> t3 = {1UL, 2UL, 5UL};
    tree.put(t1);
    tree.put(t2);
    tree.put(t3);
    std::cout << "Test 5" << std::endl;
}

void test6() {
    // rdftrie::MemRDFTree tree;
    rdftrie::MemRDFTree2 tree;
    std::vector<uint64_t> t1 = {1, 5, 6};
    std::vector<uint64_t> t2 = {2, 5, 6};
    std::vector<uint64_t> t3 = {3, 5, 6};
    tree.put(t1);
    tree.put(t2);
    tree.put(t3);
    std::vector<uint64_t> p1 = {1};
    std::set<std::vector<uint64_t>> ret;
    tree.get(p1, ret);
    print_ret(ret);
    assert(true);
    std::cout << tree.count_tuples() << std::endl;
    std::cout << "Test 6" << std::endl;
}


void test7() {
    // rdftrie::MemRDFTree tree;
    rdftrie::MemRDFTree2 tree;
    std::vector<uint64_t> t1 = {1, 5, 6};
    std::vector<uint64_t> t2 = {2, 5, 6};
    std::vector<uint64_t> t3 = {3, 5, 6};
    std::vector<uint64_t> t4 = {3, 4, 7};
    tree.put(t1);
    tree.put(t2);
    tree.put(t3);
    tree.put(t4);
    std::vector<uint64_t> p1 = {0, 5, 0};
    std::set<std::vector<uint64_t>> ret;
    tree.query(p1, ret);
    print_ret(ret);
    std::cout << "Test 7" << std::endl;
}


int main(int argc, char const *argv[])
{
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    return 0;
}
