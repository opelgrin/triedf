#include "dictionary.hpp"
#include "dict_map.hpp"
#include "mem_dict_trie.hpp"

namespace rdftrie 
{

    class MemDictionary: Dictionary 
    {

        private:
            uint64_t __current_id;

            // MemDictTrie __uri_dict;
            MemDictTrie2 __uri_dict;
            DictMap __oth_dict;

        public:
            MemDictionary();
            MemDictionary(uint64_t start_id);

            uint64_t string_to_id(const Term term);
            std::string id_to_string(const uint64_t id);

    };


    class MapMemDictionary: Dictionary
    {

        private:
            uint64_t __current_id;

            DictMap __dict;

        public:
            MapMemDictionary();
            MapMemDictionary(uint64_t start_id);

            uint64_t string_to_id(const Term term);
            std::string id_to_string(const uint64_t id);

    };

}