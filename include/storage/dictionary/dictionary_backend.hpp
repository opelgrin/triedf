#pragma once

#include <cstdint>
#include <string>

namespace rdftrie 
{

    class DictionaryBackend 
    {
        public:
            
            virtual uint64_t string_to_id(std::string str, uint64_t id, bool no_insert = false) = 0;
            virtual std::string id_to_string(uint64_t id) = 0;

            virtual ~DictionaryBackend() = default;

    };


    class DictionaryBackend2 
    {
        public:
            virtual uintptr_t string_to_id(std::string str, bool no_insert = false) = 0;
            virtual std::string id_to_string(uintptr_t id) = 0;

            virtual ~DictionaryBackend2() = default;

    };

}