#pragma once

#include <string>
#include <vector>
#include <map>
#include <mutex>
#include <shared_mutex>

#include "storage/dictionary/dictionary.hpp"


namespace rdftrie {


    class TrieNode {

        public:

            virtual TrieNode* get_child(std::string& key) const = 0;
            virtual TrieNode* make_child(std::string key, bool make_leaf = false) = 0;

            virtual std::string get_key() const = 0;
            virtual void set_key(const std::string* key) = 0;

            virtual const TrieNode* get_parent() const = 0;
            virtual void set_parent(const TrieNode* parent) = 0;

            virtual ~TrieNode() = default;
    
    };


    class MemTrieNode: public TrieNode {

        private:
            const MemTrieNode* __parent;
            const std::string* __key;
            std::map<std::string, MemTrieNode>* __children;

        public:
            MemTrieNode();

            TrieNode* get_child(std::string& key) const override;
            TrieNode* make_child(std::string key, bool make_leaf = false) override;

            std::string get_key() const override;
            void set_key(const std::string* key) override;

            const TrieNode* get_parent() const override;
            void set_parent(const TrieNode* parent) override;
            
            ~MemTrieNode();

    };


    class MemLeafNode: public TrieNode {

        private:
            const std::string* __key;

        public:
            MemLeafNode();

            TrieNode* get_child(std::string& key) const override;
            TrieNode* make_child(std::string key, bool make_leaf = false) override;

            std::string get_key() const override;
            void set_key(const std::string* key) override;

            const TrieNode* get_parent() const override;
            void set_parent(const TrieNode* parent) override;

    };


    class MemRootNode: public TrieNode {
        
        private:
            std::map<std::string, MemTrieNode> __children;
            std::map<std::string, MemLeafNode> __children_leaf;
        
        public:
            TrieNode* get_child(std::string& key) const override;
            TrieNode* make_child(std::string key, bool make_leaf = false) override;

            std::string get_key() const override;
            void set_key(const std::string* key) override;

            const TrieNode* get_parent() const override;
            void set_parent(const TrieNode* parent) override;
    
    };


    class MemTrieDictionary: Dictionary2 {

        private:
            MemRootNode __root;

            std::unique_ptr<std::shared_mutex> __mutex;

            uintptr_t __set_uri(const std::vector<std::string>& elements, bool no_insert = false);
            uintptr_t __set_other(std::string element, bool no_insert = false);
            void __get(uintptr_t id, std::vector<std::string>& ret);

        public:
            MemTrieDictionary();

            uintptr_t string_to_id(const Term term);
            std::string id_to_string(uintptr_t id);

    };

}