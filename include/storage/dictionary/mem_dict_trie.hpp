#pragma once

#include <memory>
#include <mutex>
#include <shared_mutex>
#include <unordered_map>

#include "dict_trie.hpp"
#include "storage/stores/trees/mem_bidirectional_node.hpp"

namespace rdftrie 
{

    class MemDictTrie: public DictTrie
    {        
        private:
            uint64_t __node_id;

            std::unordered_map<uint64_t, MemBidirectionalNode> __nodes;
            std::unordered_map<uint64_t, uint64_t> __rev_trie;

            uint64_t __get_new_nodeid();

            uint64_t __set(const std::vector<std::string> elements, uint64_t id, bool no_insert = false);
            std::vector<std::string> __get(uint64_t id);
        
        public:        
            MemDictTrie();

            uint64_t string_to_id(std::string str, uint64_t id, bool no_insert = false);
            std::string id_to_string(const uint64_t id);
    };


    class MemDictTrie2: public DictTrie
    {
        private:
            MemBidirectionalNode2 __root;

            std::unique_ptr<std::shared_mutex> __mutex;

            std::unordered_map<uint64_t, MemBidirectionalNode2*> __rev_trie;

            uint64_t __set(const std::vector<std::string> elements, uint64_t id, bool no_insert = false);
            std::vector<std::string> __get(uint64_t id);

        public:
            MemDictTrie2();

            uint64_t string_to_id(std::string str, uint64_t id, bool no_insert = false);
            std::string id_to_string(uint64_t id);
    };


}