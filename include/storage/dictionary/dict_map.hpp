#pragma once

#include <unordered_map>
// #include <map>
#include <memory>
#include <mutex>
#include <shared_mutex>

#include "dictionary_backend.hpp"

namespace rdftrie {

    class DictMap: DictionaryBackend {

        private:
            std::unique_ptr<std::shared_mutex> __mutex;

            std::unordered_map<std::string, uint64_t> __str_id;
            std::unordered_map<uint64_t, const char*> __id_str;
            // std::map<std::string, uint64_t> __str_id;
            // std::map<uint64_t, const char*> __id_str;

        public:
            DictMap();

            uint64_t string_to_id(std::string str, uint64_t id, bool no_insert = false);
            std::string id_to_string(uint64_t id);

    };

}