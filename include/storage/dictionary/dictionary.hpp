#pragma once
 
#include <cstdint>
#include <string>

#include "rdf/term/rdf_term.hpp"

namespace rdftrie {

    class Dictionary {

        public:

            virtual uint64_t string_to_id(const Term term) = 0;
            virtual std::string id_to_string(const uint64_t id) = 0;

            virtual ~Dictionary() = default;

    };

    class Dictionary2 {

        public:

            virtual uintptr_t string_to_id(const Term term) = 0;
            virtual std::string id_to_string(uintptr_t id) = 0;

            virtual ~Dictionary2() = default;

    };

}
