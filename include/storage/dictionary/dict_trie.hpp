#pragma once

#include <vector>

#include "storage/bin_data.hpp"
#include "dictionary_backend.hpp"

namespace rdftrie {

    class DictTrie: DictionaryBackend {

        public:

            virtual uint64_t string_to_id(std::string str, uint64_t id, bool no_insert = false) = 0;
            virtual std::string id_to_string(uint64_t id) = 0; 

    };


    class DictTrie2: DictionaryBackend2 {

        public:

            virtual uintptr_t string_to_id(std::string str, bool no_insert = false) = 0;
            virtual std::string id_to_string(uintptr_t id) = 0; 

    };

}