#pragma once

#include <fstream>
#include <cstdlib>

#include "bin_data.hpp"

namespace rdftrie {

    class File {

        private:
        
            std::fstream __file;

        public:

            void open(std::string path);
            void close();

            void read(size_t offset, char* buffer, size_t size);
            void read(size_t offset, BinData& data, size_t size);
            void write(size_t offset, char* buffer, size_t size);

    };

}