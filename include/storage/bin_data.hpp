#pragma once

#include <cstdlib>
#include <string>
#include <vector>

namespace rdftrie {
    
    class BinData {

        private:

            size_t __size;
            char* __data;

        public:

            BinData();
            BinData(const std::string& data);
            BinData(const std::vector<char>& data);
            BinData(const char* data, size_t size);
            BinData(const uint64_t data);
            BinData(const BinData& other);
            ~BinData();

            const char* get_data() const;
            void set_data(char* data, size_t size);
            const size_t get_size() const;
            
            bool operator==(const BinData& other) const;
            bool operator!=(const BinData& other) const;
            bool operator<(const BinData& other) const;

            size_t get_hash() const;

            uint64_t to_uint64();
    };

}

namespace std {

    template <>
    struct hash<rdftrie::BinData>
    {
        std::size_t operator()(const rdftrie::BinData& k) const {
            return k.get_hash();
        }
    };

}
