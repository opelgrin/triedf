#pragma once

#include <vector>
#include <set>
#include <cstdint>

namespace rdftrie{

    class RDFTree {

        public:

            virtual void put(std::vector<uintptr_t> values) = 0;
            virtual void get(std::vector<uintptr_t> values, std::set<std::vector<uintptr_t>>& tuples) = 0;
            virtual bool exist(std::vector<uintptr_t> values) = 0;
            virtual uint64_t count_tuples() = 0;

            virtual void query(const std::vector<uintptr_t>& pattern, std::set<std::vector<uintptr_t>>& tuples) = 0;

            virtual ~RDFTree() {}

    };

}