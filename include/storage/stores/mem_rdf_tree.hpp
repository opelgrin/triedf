#pragma once

#include <unordered_map>

#include "rdf_tree.hpp"
#include "trees/mem_node.hpp"

namespace rdftrie {

    class MemRDFTree: RDFTree {

        private:
            uint64_t __node_id;
            std::unordered_map<uint64_t, MemNode> __nodes;

            uint64_t __get_new_nodeid();

            void __get_node_children_rec(uint64_t node_id, std::vector<uint64_t> path, std::set<std::vector<uint64_t>>& results);
            void __rec_count(uint64_t node_id, uint64_t& count);
            void __rec_query(std::vector<uint64_t> query, unsigned query_ind, uint64_t node_id, std::vector<uint64_t> path, std::set<std::vector<uint64_t>>& results);

        public: 
            MemRDFTree();
            MemRDFTree(const uint64_t start_node_id);

            void put(std::vector<uint64_t> values) override;
            void get(std::vector<uint64_t> values, std::set<std::vector<uint64_t>>& tuples) override;
            bool exist(std::vector<uint64_t> values) override;
            uint64_t count_tuples() override;

            void query(const std::vector<uint64_t>& pattern, std::set<std::vector<uint64_t>>& tuples) override;

    };

}