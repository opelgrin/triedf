#pragma once

#include "bin_data.hpp"

namespace rdftrie {

    class KVStore {

        public:

            virtual void put(BinData& key, BinData& val, BinData& ret) = 0;
            virtual void get(BinData& key, BinData& ret) = 0;

    };

}