#pragma once

// #include <unordered_map>
#include <map>
#include "storage/stores/trees/children.hpp"

namespace rdftrie {

    template<class T>
    class MemChildren: Children<T> {

        private:

            std::map<T, uint64_t> __children;

        public:

            const T* set(T key, uint64_t node_id) {
                auto ret = this->__children.insert(std::make_pair(key, node_id));
                return &(*(ret.first)).first;
            }

            uint64_t get(T key) {
                uint64_t return_val = 0;
                if (this->__children.find(key) != this->__children.end()) {
                    return_val = this->__children[key];
                }
                return return_val;
            }
            
            std::vector<std::pair<T, uint64_t>> list() {
                std::vector<std::pair<T, uint64_t>> values;
                for (auto kv: this->__children) {
                    values.push_back(kv);
                }
                return values;
            }

    };

}