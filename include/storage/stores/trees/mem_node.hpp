#pragma once

#include "node.hpp"
#include "mem_children.hpp"

namespace rdftrie
{
    class MemNode: Node {

        private:

            MemChildren<uint64_t>* __children;

        public:

            MemNode();

            uint64_t get_child(const uint64_t key);
            void add_child(const uint64_t key, uint64_t node_id);
            std::vector<std::pair<uint64_t, uint64_t>> get_children();

            uint64_t num_children();
            
            uint64_t get_value();

            ~MemNode();
    };

} 
