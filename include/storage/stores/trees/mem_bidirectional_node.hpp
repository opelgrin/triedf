#pragma once

#include <string>

#include "mem_children.hpp"
#include "bidirectional_node.hpp"

namespace rdftrie 
{

    class MemBidirectionalNode: public BidirectionalNode
    {
        private:

            uint64_t __value;
            uint64_t __parent_id;
            MemChildren<std::string>* __children;
            const char* __key;

        public:

            MemBidirectionalNode();
            // MemBidirectionalNode(std::string key, uint64_t parent);
            // MemBidirectionalNode(std::string key, uint64_t value, uint64_t parent);
            MemBidirectionalNode(const char* key, uint64_t parent);
            MemBidirectionalNode(const char* key, uint64_t value, uint64_t parent);
            MemBidirectionalNode(uint64_t parent);

            uint64_t get_child(const std::string key);
            const char* add_child(const std::string key, const uint64_t node_id);
            std::vector<std::pair<std::string, uint64_t>> get_children();

            std::string get_key();
            uint64_t get_value();
            uint64_t get_parent();
            void set_value(uint64_t value);
            void set_parent(uint64_t parent_id);
            void set_key(const char* key);

            ~MemBidirectionalNode();

    };


    class MemBidirectionalNode2
    {
        private:
            uint64_t __value;
            const MemBidirectionalNode2* __parent;
            std::map<std::string, MemBidirectionalNode2>* __children;
            const std::string* __key;

        public:
            MemBidirectionalNode2();
            MemBidirectionalNode2(const std::string* key, const MemBidirectionalNode2* parent);
            MemBidirectionalNode2(const std::string* key, uint64_t value, const MemBidirectionalNode2* parent);
            MemBidirectionalNode2(const MemBidirectionalNode2* parent);

            MemBidirectionalNode2* get_child(std::string& key) const;
            MemBidirectionalNode2* make_child(std::string key);

            std::string get_key() const;
            uint64_t get_value() const;
            const MemBidirectionalNode2* get_parent() const;
            void set_value(uint64_t value);
            void set_parent(const MemBidirectionalNode2* parent);
            void set_key(const std::string* key);

            ~MemBidirectionalNode2();
    };


    class MemBidirectionalLeafNode: BidirectionalNode2
    {
        private:
            uint64_t __value;
            const std::string* __key;

        public:
            MemBidirectionalLeafNode();
            MemBidirectionalLeafNode(const std::string* key, uint64_t value);

            BidirectionalNode2* get_child(std::string& key) const;
            BidirectionalNode2* make_child(std::string key, bool make_leaf = false);

            std::string get_key() const;
            uint64_t get_value() const;
            const BidirectionalNode2* get_parent() const;
            void set_value(uint64_t value);
            void set_parent(const BidirectionalNode2* parent);
            void set_key(const std::string* key);
    };

}