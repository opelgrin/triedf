#pragma once

#include "storage/bin_data.hpp"

namespace rdftrie
{
    template<class T>
    class Children {

        public:

            // Here "node_id" is an integer value that can be used to find the Node
            // It may be a simple ID in a dict, or offset in a file, or BlockID (?)...
            virtual const T* set(T key, uint64_t node_id) = 0;
            virtual uint64_t get(T key) = 0;

            virtual std::vector<std::pair<T, uint64_t>> list() = 0;

            virtual ~Children() {}

    };
}
