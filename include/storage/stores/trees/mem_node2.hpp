#pragma once

#include <map>
#include <set>
#include <cstdint>
#include <memory>


namespace rdftrie {

    // class MemNode2 {
    //     private:
    //         std::unique_ptr<std::map<uintptr_t, std::unique_ptr<MemNode2>>> __children;

    //     public:
    //         // Return a pair(true, Node) if found else (false, nullptr)
    //         std::pair<bool, MemNode2*> get_child(uintptr_t key);
    //         MemNode2* make_child(uintptr_t key, bool make_leaf = false);

    //         bool has_children();

    //         std::map<uintptr_t, std::unique_ptr<MemNode2>>::const_iterator begin();
    //         std::map<uintptr_t, std::unique_ptr<MemNode2>>::const_iterator end();

    // };

    class MemNode2 {
        private:
            std::unique_ptr<std::map<uintptr_t, MemNode2>> __children;

        public:
            // Return a pair(true, Node) if found else (false, nullptr)
            std::pair<bool, MemNode2*> get_child(uintptr_t key) const;
            MemNode2* make_child(uintptr_t key, bool make_leaf = false);

            bool has_children() const;

            std::map<uintptr_t, MemNode2>::iterator begin();
            std::map<uintptr_t, MemNode2>::iterator end();

    };

}