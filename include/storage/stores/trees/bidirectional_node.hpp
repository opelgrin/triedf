#pragma once

namespace rdftrie 
{

    class BidirectionalNode
    {
        
        public:

            // virtual uint64_t get_child(const BinData key) = 0;
            virtual uint64_t get_child(const std::string key) = 0;
            // virtual void add_child(const BinData key, const uint64_t node_id) = 0;
            virtual const char* add_child(const std::string key, const uint64_t node_id) = 0;
            // virtual std::vector<std::pair<BinData, uint64_t>> get_children() = 0;
            virtual std::vector<std::pair<std::string, uint64_t>> get_children() = 0;

            // virtual BinData get_key() = 0;
            virtual std::string get_key() = 0;
            virtual uint64_t get_value() = 0;
            virtual void set_value(uint64_t value) = 0;
            virtual uint64_t get_parent() = 0;

            virtual ~BidirectionalNode() = default;

    };


    class BidirectionalNode2
    {
        
        public:

            virtual BidirectionalNode2* get_child(std::string& key) const = 0;
            virtual BidirectionalNode2* make_child(std::string key, bool make_leaf = false) = 0;

            virtual std::string get_key() const = 0;
            virtual uintptr_t get_value() const = 0;
            virtual const BidirectionalNode2* get_parent() const = 0;
            virtual void set_value(uintptr_t value) = 0;
            virtual void set_parent(const BidirectionalNode2* parent) = 0;
            virtual void set_key(const std::string* key) = 0;

            virtual ~BidirectionalNode2() = default;

    };


}