#pragma once

#include "storage/bin_data.hpp"

namespace rdftrie {

    class Node {

        public:

            // The way childrens are stored is up to the implementation
            virtual uint64_t get_child(const uint64_t key) = 0;
            virtual void add_child(const uint64_t key, const uint64_t node_id) = 0;
            virtual std::vector<std::pair<uint64_t, uint64_t>> get_children() = 0;

            // May not be used for every types of Trees
            virtual uint64_t get_value() = 0;

            virtual ~Node() {}
            
    };

}