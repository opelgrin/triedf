#pragma once

#include <memory>
#include <mutex>
#include <shared_mutex>

#include "rdf_tree.hpp"
#include "trees/mem_node2.hpp"


namespace rdftrie {

    class MemRDFTree2: RDFTree {
        private:
            MemNode2 __root;

            std::unique_ptr<std::shared_mutex> __mutex;

            void __get_node_children_rec(MemNode2* node, std::vector<uintptr_t> path, std::set<std::vector<uintptr_t>>& results);
            void __rec_count(MemNode2* node, uint64_t& count);
            void __rec_query(std::vector<uintptr_t> query, unsigned query_ind, MemNode2* node, std::vector<uintptr_t> path, std::set<std::vector<uintptr_t>>& results);

        public:
            MemRDFTree2();

            void put(std::vector<uintptr_t> values) override;
            void get(std::vector<uintptr_t> values, std::set<std::vector<uintptr_t>>& tuples) override;
            bool exist(std::vector<uintptr_t> values) override;
            uint64_t count_tuples() override;

            void query(const std::vector<uintptr_t>& pattern, std::set<std::vector<uintptr_t>>& tuples) override;

    };

}