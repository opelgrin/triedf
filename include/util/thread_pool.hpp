#pragma once

#include <thread>
#include <vector>
#include <queue>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <unordered_map>

namespace rdftrie {

    // A basic thread pool implementation. Inspired by this post https://stackoverflow.com/a/29742586
    class ThreadPool {
        
        public:

            ThreadPool();
            ThreadPool(unsigned n_threads);
            ~ThreadPool();

            // Submit a job for execution on a thread
            void submit_job(std::function<void(void)> job);
            
            // Submit a list of jobs for execution
            void submit_jobs(std::vector<std::function<void(void)>> jobs);

            // Submit a list of jobs and wait for all of them to end
            // In this case, the main thread will go to sleep after submiting the jobs
            void submit_jobs_and_wait(std::vector<std::function<void(void)>> jobs);

            void wait_completion();

            size_t get_num_workers();

        protected:

            std::queue<std::function<void(void)>> jobs;
            std::vector<std::thread> worker_threads;
            
            std::mutex lock;
            std::condition_variable cond_var;
            
            std::unordered_map<std::thread::id, bool> work_status;
            std::mutex wlock;
            std::condition_variable wcv;
            bool is_pool_busy();

            bool shutdown = false;

            // Main loop for each thread. They will look for work in the jobs queue.
            void thread_main_loop();

    };

}
