import sys
import itertools

header = '#pragma once\n\nnamespace rdftrie {\n\n'
footer = '}'


def list_to_str(l: list) -> str:
    s = ''
    for e in l:
        s += e
    return s


def to_cpp(letters: str) -> str:
    permu = list(itertools.permutations(letters))
    cpp = ''
    for p in permu:
        cpp += f'\n\t\t{list_to_str(p).upper()},'
    return cpp


def main():    
    if (len(sys.argv) < 2):
        print('Missing args: <letters> ...')
        sys.exit()
    
    cpp = header
    cpp += '\tenum tuple_order {'
    for i in range(1, len(sys.argv)):
        cpp += to_cpp(sys.argv[i])
    cpp = cpp[:-1] + '\n\t};'
    cpp += f'\n\n\n{footer}'

    f = open('./enum.hpp', 'w', encoding='utf-8')
    f.write(cpp)
    f.close()


if __name__ == "__main__":
    main()
