#pragma once

namespace rdftrie {

	enum tuple_order {
		SPO,
		SOP,
		PSO,
		POS,
		OSP,
		OPS,
		SPOV,
		SPVO,
		SOPV,
		SOVP,
		SVPO,
		SVOP,
		PSOV,
		PSVO,
		POSV,
		POVS,
		PVSO,
		PVOS,
		OSPV,
		OSVP,
		OPSV,
		OPVS,
		OVSP,
		OVPS,
		VSPO,
		VSOP,
		VPSO,
		VPOS,
		VOSP,
		VOPS
	};


}