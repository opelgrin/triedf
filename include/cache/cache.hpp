#pragma once

namespace rdftrie {

    template <class K, class V>
    class Cache 
    {

    public:

        virtual Cache(unsigned capacity) = 0;
        virtual ~Cache() = default;

        virtual void set(K key, V value) = 0;
        virtual void get(K key) = 0;
        virtual void del(K key) = 0;
        
        virtual unsigned get_current_size() = 0;
        virtual unsigned get_max_size() = 0;

    };

}

