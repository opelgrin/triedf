#pragma once

#include <unordered_map>
#include <list>
#include "cache.hpp"


namespace rdftrie {


    template<class K, class V>
    class CacheNode;


    template <class K, class V>
    class FrequencyNode {
    
    private:
        unsigned __frequency;
        FrequencyNode<K, V> *__prev;
        FrequencyNode<K, V> *__next;
        CacheNode<K, V> *__head;
        CacheNode<K, V> *__tail;
    
    public:
        FrequencyNode();
        ~FrequencyNode();
        
        unsigned get_frequency();
        
        void append_cache_tail(CacheNode<K, V> *c_node);
        void pop_cache_head();
        
        void insert_after(FrequencyNode<K, V> *freq_node);
        void insert_before(FrequencyNode<K, V> *freq_node);
        
        CacheNode<K, V>* get_head();
        CacheNode<K, V>* get_tail();
        void set_head(CacheNode<K, V>* new_head);
        void set_tail(CacheNode<K, V>* new_tail);
        
        void remove();    
    };


    template<class K, class V>
    class CacheNode {

    private:

        K __key;
        V __value;
        FrequencyNode <K, V> *__parent_fnode;
        CacheNode<K, V> *__next;
        CacheNode<K, V> *__prev;

    public:

        CacheNode(K key, V value) : __parent_fnode(nullptr), __next(nullptr), __prev(nullptr) {
            this->__key = key;
            this->__value = value;
        }

        K get_key() {
            return this->__key;
        }

        V get_value() {
            return this->__value;
        }

        FrequencyNode <K, V> *get_parent_fnode() {
            return this->__parent_fnode;
        }

        void set_parent_fnode(FrequencyNode <K, V> *new_fnode) {
            this->__parent_fnode = new_fnode;
        }

        CacheNode<K, V> *get_next() {
            return this->__next;
        }

        void set_next(CacheNode<K, V> *new_next) {
            this->__next = new_next;
        }

        CacheNode<K, V> *get_prev() {
            return this->__prev;
        }

        void set_prev(CacheNode<K, V> *new_prev) {
            this->__prev = new_prev;
        }

        void clear() {
            if (this->__parent_fnode->get_head() == this->__parent_fnode->get_tail()) {
                this->__parent_fnode->set_head(nullptr);
                this->__parent_fnode->set_tail(nullptr);
            } else if (this->__parent_fnode->get_head() == this) {
                this->__next->set_prev(nullptr);
                this->__parent_fnode->set_head(this->__next);
            } else if (this->__parent_fnode->get_tail() == this) {
                this->__prev->set_next(nullptr);
                this->__parent_fnode->set_tail(this->__prev);
            } else {
                this->__prev->set_next(this->__next);
                this->__next->set_prev(this->__prev);
            }
            this->__next = nullptr;
            this->__prev = nullptr;
            this->__parent_fnode = nullptr;
        }

    };


    template<class K, class V>
    class FrequencyNode {

    private:

        unsigned __frequency;
        FrequencyNode<K, V> *__prev;
        FrequencyNode<K, V> * __next;
        CacheNode<K, V> *__head;
        CacheNode<K, V> *__tail;

    public:

        FrequencyNode(unsigned frequency) {
            this->__frequency = frequency;
            this->__prev = nullptr;
            this->__next = nullptr;
            this->__head = nullptr;
            this->__tail = nullptr;
        }

        FrequencyNode<K, V>* get_prev() {
            return this->__prev;
        }

        void set_prev(FrequencyNode<K, V>* new_prev) {
            this->__prev = new_prev;
        }

        FrequencyNode<K, V>* get_next() {
            return this->__next;
        }

        void set_next(FrequencyNode<K, V>* new_next) {
            this->__next = new_next;
        }
        
        void append_cache_tail(CacheNode<K, V> *c_node) {
            c_node->set_parent_fnode(this);
            if (this->__head == nullptr && this->__tail == nullptr) {
                this->__head = c_node;
                this->__tail = c_node;
            } else {
                c_node->set_prev(this->__head);
                c_node->set_next(nullptr);
                this->__tail->set_next(c_node);
                this->__tail = c_node;
            }
        }

        CacheNode<K, V>* pop_cache_head() {
            if (this->__head == nullptr && this->__tail == nullptr) {
                return nullptr;
            }
            if (this->__head == this->__tail) {
                CacheNode<K, V>* c_head = this->__head;
                this->__head = nullptr;
                this->__tail = nullptr;
                return c_head;
            }
            CacheNode<K, V>* c_head = this->__head;
            this->__head->get_next()->set_prev(nullptr);
            this->__head = this->__head->get_next();
            return c_head;
        }

        void insert_after(FrequencyNode<K, V> *freq_node) {
            freq_node->set_prev(this);
            freq_node->set_next(this->__next);
            if (this->__next != nullptr) {
                this->__next->set_prev(freq_node);
            }
            this->__next = freq_node;
        }

        void insert_before(FrequencyNode<K, V> *freq_node) {
            if (this->__prev != nullptr) {
                this->__prev->set_next(freq_node);
            }
            freq_node->set_prev(this->__prev);
            freq_node->set_next(this);
            this->__prev = freq_node;
        }

        void remove() {
            if (this->__prev != nullptr) {
                this->__prev->set_next(this->__next);
            }
            if (this->__next != nullptr) {
                this->__next->set_prev(this->__prev);
            }
            this->__prev = nullptr;
            this->__next = nullptr;
            this->__head = nullptr;
            this->__tail = nullptr;
        }

        CacheNode<K, V>* get_head() {
            return this->__head;
        }

        CacheNode<K, V>* set_head(CacheNode<K, V>* new_head) {
            this->__head = new_head;
        }

        CacheNode<K, V>* get_tail() {
            return this->__tail;
        }

        CacheNode<K, V>* set_tail(CacheNode<K, V>* new_tail) {
            this->__tail = new_tail;
        }

    };


    template<class K, class V>
    class LFUCache : Cache<K, V> {

    private:

        unsigned __capacity;
        std::unordered_map <CacheNode<K, V>> __cache;
        FrequencyNode<K, V> *__freq_list;

        unsigned __cache_hit;
        unsigned __cache_miss;
        unsigned __evictions;

        void move_forward(CacheNode<K, V> *c_node) {}

        void evict() {}

        void insert_cache(K key, V value) {}


    public:

        LFUCache(unsigned capacity) {
            this->__capacity = capacity;
        }

        void set(K key, V value) {}

        void get(K key) {}

        void del(K key) {}

        unsigned get_current_size() {}

        unsigned get_max_size() {}

    };


}