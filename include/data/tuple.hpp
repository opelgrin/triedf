#pragma once

#include <vector>
#include "rdf_term.hpp"

namespace rdftrie {

    class Tuple {

        private:

            std::vector<Term> __tuple;
        
        public:

            Term get_term(unsigned index);
            std::vector<Term> get_terms();

    };

}