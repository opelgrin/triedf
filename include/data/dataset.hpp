#pragma once

#include <vector>
#include <string>

#include "rdf_term.hpp"

namespace rdftrie {

    class Dataset {

        public:

            virtual void add(std::vector<Term> tuple) = 0;
            virtual void add(std::vector<std::vector<Term>> tuples) = 0;

            virtual void get(std::vector<std::string> pattern) = 0;

    };

}