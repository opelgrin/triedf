#pragma once

#include <regex>
#include <vector>

#include "rdf/term/rdf_term.hpp"


namespace rdftrie {

    class FiveSink {

        public:

            virtual void tuple(Term s, Term p, Term o, Term v, Term src) = 0;
            virtual ~FiveSink() = default;

    };

    class FiveTupleParser {

        public:
            FiveTupleParser();
            FiveTupleParser(FiveSink* sink);

            void parse(const std::string file_path);

            void parse_string(const std::string str, std::vector<std::vector<Term>>& tuples);


        private:

            FiveSink* __sink;

            static const std::string __string;
            static const std::string __wspaces;
            static const std::string __version;

            std::regex __string_re;
            std::regex __wspaces_re;
            std::regex __version_re;

            std::string __current_line;

            void parseline();
            
            Term string(rdf_term_type term_type);
            Term string(std::string& line, rdf_term_type term_type);
            Term version();
            Term version(std::string& line);

            void eat_whitespaces();
            void eat_whitespaces(std::string& line);

    };

    class FiveTupleParserFast {

        public:
            FiveTupleParserFast();
            FiveTupleParserFast(FiveSink* sink);

            void parse(const std::string file_path);

            void parse_string(const std::string str, std::vector<std::vector<Term>>& tuples);


        private:

            FiveSink* __sink;

            std::string __sep;
            std::string __current_line;

            void parseline();

    };

}
