#pragma once

#include <regex>
#include <vector>

#include "rdf/term/rdf_term.hpp"

namespace rdftrie {

    class TripleSink {

        public:

            virtual void triple(Term s, Term p, Term o) = 0;
            virtual ~TripleSink() = default;

    };

    class NTParser {

        public:
            NTParser();
            NTParser(TripleSink* sink);

            void parse(const std::string file_path);

            void parse_string(const std::string str, std::vector<std::vector<Term>>& triples);

        private:

            TripleSink* sink;

            static const std::string uriref;
            static const std::string literal_string;
            static const std::string literal_info;
            static const std::string literal;
            static const std::string wspaces;
            static const std::string node_id;

            std::regex uriref_re;
            std::regex literal_re;
            std::regex wspaces_re;
            std::regex node_id_re;

            std::string current_line;

            void parseline();

            Term subject();
            Term subject(std::string& line);
            Term predicate();
            Term predicate(std::string& line);
            Term object();
            Term object(std::string& line);

            void eat_whitespaces();
            void eat_whitespaces(std::string& line);

        };

}
