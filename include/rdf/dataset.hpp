#pragma once

#include <unordered_map>
#include <vector>
#include <set>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <memory>

#include "rdf/term/rdf_term.hpp"
#include "pattern.hpp"
#include "storage/dictionary/trie_dictionary.hpp"
#include "storage/stores/mem_rdf_tree2.hpp"
#include "util/thread_pool.hpp"
#include "rdf/parser/ntparser.hpp"


namespace rdftrie {

    class Dataset {

        public:

            virtual void add_tuple(std::vector<Term>& tuple) = 0;            
            virtual void add_tuples(std::vector<std::vector<Term>>& tuples) = 0;

            virtual void load_from_file(const std::string filename) = 0;

            virtual void query(Pattern& pattern, std::set<std::vector<std::string>>& results) = 0;

            virtual ~Dataset() = default;

    };


    class MemTripleDataset: Dataset {

        private:
            std::unordered_map<tuple_order, MemRDFTree2> __trees;
            MemTrieDictionary __dict;

            bool __pattern_to_int(Pattern& p, std::vector<uint64_t>& p2);
            void __triple_to_string(std::vector<uint64_t>& triple_int, std::vector<std::string>& triple_str);

            void __unserialize_triples(std::set<std::vector<uint64_t>>& triples_int, std::set<std::vector<std::string>>& triples_str, tuple_order query_order);
            void __unserialize_triples_parallel(std::set<std::vector<uint64_t>>& triples_int, std::set<std::vector<std::string>>& triples_str, tuple_order query_order);

            // tuple_order get_index(tuple_order pref);
            tuple_order get_index(Pattern& p);

            // Parallel setup
            bool __use_mt;
            void __insert_tuple(std::vector<Term>& tuple);

            NTParser __ntparser;
            void __parse_and_insert(std::string str);
            // void __bulk_parse_and_insert(std::string str);

        public:
            MemTripleDataset();
            MemTripleDataset(std::vector<tuple_order> add_orders);

            void add_tuple(std::vector<Term>& tuple) override;            
            void add_tuples(std::vector<std::vector<Term>>& tuples) override;

            void load_from_file(const std::string filename);

            void query(Pattern& pattern, std::set<std::vector<std::string>>& results) override;
            uint64_t timed_query(Pattern& pattern);

            void terminate_insertion();
            void prepare_insertion();

    };

}