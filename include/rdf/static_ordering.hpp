#pragma once

#include <cstdlib>
#include <vector>
#include <cstdint>
#include <string>

// Everything here should be replaced by something better and more dynamic later on

namespace rdftrie {

	enum tuple_order {
		SPO,
		SOP,
		PSO,
		POS,
		OSP,
		OPS,
		SPOV,
		SOPV,
		PSOV,
		POSV,
		OSPV,
		OPSV,
		VSPO,
		VSOP,
		VPSO,
		VPOS,
		VOSP,
		VOPS,
		SPORV,
		SOPRV,
		PSORV,
		POSRV,
		OSPRV,
		OPSRV,
	};

	// Assume that the original tuple order is SPO
	std::vector<uint64_t> reorder_triple(const std::vector<uint64_t>& t1, tuple_order new_order);
	std::vector<uint64_t> reorder_to_spo(const std::vector<uint64_t>& t1, tuple_order old_order);
	tuple_order gen_pref_order(const std::vector<std::string>& pattern);

	std::vector<uintptr_t> reorder_to_spov(const std::vector<uintptr_t>& t1, tuple_order old_order);
	std::vector<uintptr_t> reorder_vertriple(const std::vector<uintptr_t>& t1, tuple_order new_order);

	std::vector<uintptr_t> reorder_to_sporv(const std::vector<uintptr_t>& t1, tuple_order old_order);
	std::vector<uintptr_t> reorder_verquad(const std::vector<uintptr_t>& t1, tuple_order new_order);
}

namespace std {

    template <>
    struct hash<rdftrie::tuple_order> 
    {
        size_t operator() (const rdftrie::tuple_order &t) const { 
            return size_t(t); 
        }
    };
  
}