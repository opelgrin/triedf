#pragma once

#include <vector>
#include <unordered_map>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <memory>
#include <set>

#include "rdf/term/rdf_term.hpp"
#include "pattern.hpp"
#include "storage/stores/mem_rdf_tree2.hpp"
#include "storage/dictionary/trie_dictionary.hpp"
#include "rdf/parser/fivetuples_parser.hpp"


namespace rdftrie {

    class QuadVersionedDataset {

        public:

            virtual void add_tuple(std::vector<Term>& tuple) = 0;

            // For all query type, the pattern represent a triple pattern, version information is provided separatly

            // Query last revision of the dataset
            virtual void query(Pattern& pattern, std::set<std::vector<std::string>>& results) = 0;
            // Query a single version vid
            virtual void query_vm(Pattern& pattern, uintptr_t vid, std::set<std::vector<std::string>>& results) = 0;
            // Query and qet all version where the Pattern apply
            virtual void query_vq(Pattern& pattern, std::set<std::vector<std::string>>& results) = 0;
            // Query and output the result change between vid_1 and vid_2
            virtual void query_dm(Pattern& pattern, uintptr_t vid_1, uintptr_t vid_2, std::set<std::vector<std::string>>& results_add, std::set<std::vector<std::string>>& results_del) = 0;
    
            virtual ~QuadVersionedDataset() = default;
    };


    // Indexes: XXXRV
    class MemQuadVersionedDataset: QuadVersionedDataset {

        private:
            uintptr_t __current_vid;

            std::unordered_map<tuple_order, MemRDFTree2> __trees;
            MemTrieDictionary  __dict;

            bool __pattern_to_int(Pattern& p, std::vector<uintptr_t>& p2);
            void __tuple_to_string(std::vector<uintptr_t>& tuple_int, std::vector<std::string>& tuple_str);

            void __unserialize_tuples(std::set<std::vector<uintptr_t>>& tuples_int, std::set<std::vector<std::string>>& tuples_str, tuple_order query_order);
            void __unserialize_tuples_parallel(std::set<std::vector<uintptr_t>>& tuples_int, std::set<std::vector<std::string>>& tuples_str, tuple_order query_order);

            void __compute_delta(std::set<std::vector<uintptr_t>>& r1, std::set<std::vector<uintptr_t>>& r2, std::set<std::vector<uintptr_t>>& added, std::set<std::vector<uintptr_t>>& deleted);

            tuple_order to_xxxrv(tuple_order order);

            tuple_order get_index_dm(const std::vector<std::string>& pattern);
            tuple_order get_index_vq(const std::vector<std::string>& pattern);
            tuple_order get_index_vm(const std::vector<std::string>& pattern);

            void __insert_tuple(std::vector<Term>& tuple);

            FiveTupleParserFast __ntparser;
            // FiveTupleParser __ntparser;
            void __parse_and_insert(std::string str);
    
        public:
            MemQuadVersionedDataset();
            MemQuadVersionedDataset(std::vector<tuple_order> add_orders);

            void add_tuple(std::vector<Term>& tuple) override;
            
            void load_from_file(std::string filename);
            
            void query(Pattern& pattern, std::set<std::vector<std::string>>& results) override;
            void query_vm(Pattern& pattern, uintptr_t vid, std::set<std::vector<std::string>>& results) override;
            void query_vq(Pattern& pattern, std::set<std::vector<std::string>>& results) override;
            void query_dm(Pattern& pattern, uintptr_t vid_1, uintptr_t vid_2, std::set<std::vector<std::string>>& results_add, std::set<std::vector<std::string>>& results_del) override;

    };


}