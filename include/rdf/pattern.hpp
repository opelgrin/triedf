#pragma once

#include <string>
#include <vector>

#include "static_ordering.hpp"


namespace rdftrie {

    class Pattern {

        private:

            std::vector<std::string> __pattern;
            tuple_order __pref_order;

        public:
            
            Pattern(std::vector<std::string> pattern, tuple_order pref_order);

            const std::vector<std::string>& get_pattern();
            tuple_order get_preferred_order(); 

    };

}