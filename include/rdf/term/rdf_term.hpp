#pragma once

#include <string>


namespace rdftrie {
    
    enum rdf_elem_type {
        URI,
        LITERAL,
        BLANK_NODE,
        VERSION_ID,
        UNKNOWN,
        ERROR
    };

    enum rdf_term_type {
        SUBJECT,
        PREDICATE,
        OBJECT,
        VERSION,
        PROV,
        QUERY  // Used when querying as we don't really know/need to know the term type
    };

    union rdf_term_value
    {
        uintptr_t int_val;
        std::string str_val;
    };    

    class Term {

        private:
            
            std::string __text;
            // rdf_term_value __value;
            rdf_elem_type __rdf_type;
            rdf_term_type __term_type;

        public:

            std::string get_string() const;
            rdf_elem_type get_rdf_type() const;
            rdf_term_type get_term_type() const;

            Term(std::string str, rdf_elem_type rdf_type, rdf_term_type term_type);

            bool operator==(const Term& other) const;
            bool operator!=(const Term& other) const;
            bool operator<(const Term& other) const;

            size_t get_hash() const;
    };


}

namespace std {

    template <>
    struct hash<rdftrie::Term>
    {
        std::size_t operator()(const rdftrie::Term& k) const {
            return k.get_hash();
        }
    };

}