= Compiling the experimental programs

To compile the code in this folder, run:

$ cd experiments
$ meson <build_folder_name> --buildtype=release 

<build_folder_name>: the path where the executables will be located (it can be any name of your choice)
--buildtype: release|debug

Then, run:
$ ninja

inside the <build_folder_name> folder.

When the compilation is done, the executables can be found with <build_folder_name>.
