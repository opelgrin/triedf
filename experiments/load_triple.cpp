#include <iostream>
#include <chrono>

#include "rdf/parser/ntparser.hpp"
#include "rdf/dataset.hpp"


class LoadSink: public rdftrie::TripleSink {

    private:
        uint64_t __lines;
        rdftrie::MemTripleDataset* __dataset; 

    public:
        LoadSink(rdftrie::MemTripleDataset* dataset): __lines(0) {
            __dataset = dataset;
        }

        void triple(rdftrie::Term s, rdftrie::Term p, rdftrie::Term o) {
            std::vector<rdftrie::Term> tuple = {s, p, o};
            __dataset->add_tuple(tuple);
            __lines++;
            if (__lines % 100000 == 0) {
                std::cout << "\rProcessed " << this->__lines << " triples..." << std::flush;
            }
        }

        uint64_t nlines() {
            return __lines;
        }

};


void load(std::string filename) {
    std::vector<rdftrie::tuple_order> orders = {rdftrie::PSO, rdftrie::POS};
    // std::vector<rdftrie::tuple_order> orders = {rdftrie::PSO};
    rdftrie::MemTripleDataset dataset(orders);
    LoadSink sink(&dataset);
    rdftrie::NTParser p(&sink);
    auto start = std::chrono::high_resolution_clock::now();
    p.parse(filename);
    std::cout << std::endl;
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Total processed lines: " << sink.nlines() << std::endl;
    auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(end-start);
    std::cout << "Elapsed time: " << runtime.count() << " milliseconds." << std::endl;
    std::cout << "Press any key to continue..." << std::endl;
    std::cin.get();
}


int main(int argc, char const *argv[])
{
    if (argc != 2) {
        std::cout << "Missing argument: <filename>" << std::endl;
        return 0;
    }
    load(argv[1]);
    return 0;
}
