#include <chrono>
#include <string>
#include <map>
#include <memory>
#include <fstream>

#include "query_parser.hpp"
#include "rdf/parser/ntparser.hpp"
#include "rdf/dataset.hpp"

#include "tclap/CmdLine.h"

using namespace TCLAP;
using namespace std;


class LoadSink: public rdftrie::TripleSink {

    private:
        uint64_t __lines;
        rdftrie::MemTripleDataset* __dataset; 

    public:
        LoadSink(rdftrie::MemTripleDataset* dataset): __lines(0) {
            __dataset = dataset;
        }

        void triple(rdftrie::Term s, rdftrie::Term p, rdftrie::Term o) {
            std::vector<rdftrie::Term> tuple = {s, p, o};
            __dataset->add_tuple(tuple);
            __lines++;
            if (__lines % 100000 == 0) {
                std::cout << "\rProcessed " << this->__lines << " triples..." << std::flush;
            }
        }

        uint64_t nlines() {
            return __lines;
        }

};

void print_pattern(rdftrie::Pattern& p) {
    for (auto v: p.get_pattern()) {
        std::cout << v << " ";
    }
    std::cout << std::endl;
}


void load_and_query(std::string filename, std::string query_file) {
    std::string res = "";
    
    std::vector<rdftrie::tuple_order> orders = {rdftrie::POS, rdftrie::OSP};
    rdftrie::MemTripleDataset dataset(orders);
    // rdftrie::MemTripleDataset dataset;
    // LoadSink sink(&dataset);
    // rdftrie::NTParser p(&sink);
    auto start = std::chrono::high_resolution_clock::now();
    dataset.load_from_file(filename);
    // p.parse(filename);
    // dataset.terminate_insertion();
    std::cout << std::endl;
    auto end = std::chrono::high_resolution_clock::now();
    auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(end-start);
    std::cout << "Loading time: " << runtime.count() << " milliseconds." << std::endl;

    // Query
    std::cout << "Preparation of queries..." << std::endl;
    std::vector<rdftrie::Pattern> queries;
    parse_file(query_file, queries);
    auto total_qr_start = std::chrono::high_resolution_clock::now();
    for (auto query: queries) {
        std::cout << "Query : ";
        print_pattern(query);
        std::set<std::vector<std::string>> results;
        auto start_qr = std::chrono::high_resolution_clock::now();
        dataset.query(query, results);
        auto end_qr = std::chrono::high_resolution_clock::now();
        auto runtime_qr = std::chrono::duration_cast<std::chrono::nanoseconds>(end_qr-start_qr);
        res += std::to_string(runtime_qr.count()) + "\n";
        std::cout << "Query results: " << results.size() << " , runtime: " << runtime_qr.count() << " nanoseconds." << std::endl;
        
        // // Print results
        // for (auto r: results) {
        //     for (auto s: r) {
        //         std::cout << s << " ";
        //     }
        //     std::cout << std::endl;
        // }
        // std::cout << std::endl;

    }
    auto total_qr_end = std::chrono::high_resolution_clock::now();
    auto total_qr_runtime = std::chrono::duration_cast<std::chrono::milliseconds>(total_qr_end-total_qr_start);
    res += std::to_string(total_qr_runtime.count()) + "\n";
    std::cout << "Total query runtime: " << total_qr_runtime.count() << std::endl;
    res += std::to_string(runtime.count()) + "\n";

    std::ofstream outfile;
    outfile.open("rdftrie.txt", std::ios_base::app);
    outfile << res;
    outfile.close();
}

bool parse_arguments(int argc, char const *argv[],
		std::map<string, unique_ptr<ValueArg<string>>> &parsedArgs) {
    try {
    	CmdLine cmd("Utility to load and query an RDF dataset", '=', "0.1", true);

        // Define a value argument and add it to the command line.
        unique_ptr<ValueArg<string>> dataArg(new ValueArg<string>("d", "data", "Path to an RDF dataset (ntriples)", true, "",
                                 "string"));
        cmd.add(*dataArg);

		unique_ptr<ValueArg<string>> queriesArg(new ValueArg<string>("q", "queries", "Queries file", true, "",
                                 "string"));

        cmd.add(*queriesArg);

        // Parse the args
        cmd.parse(argc, argv);
        parsedArgs.insert(make_pair("data", std::move(dataArg)));
        parsedArgs.insert(make_pair("queries", std::move(queriesArg)));

        return true;

    } catch (ArgException &e) {
    	cerr << "There was a problem when parsing the program arguments" << endl;
        cerr << "Error: " << e.error() << " for argument " << e.argId() << endl;
        return false;
    }
}


int main(int argc, char const *argv[])
{
	map<string, unique_ptr<ValueArg<string>>> parsedArgs;
	if (!parse_arguments(argc, argv, parsedArgs)) return 1;

	load_and_query(parsedArgs["data"]->getValue(), parsedArgs["queries"]->getValue());

	return 0;
}
