project('rdftrie experiments', 'cpp', version : '0.1', default_options : ['cpp_std=c++17'])

incdir = [include_directories('../include')]

src_common = [
        '../src/rdf/parser/ntparser.cpp',
        '../src/rdf/mem_triple_dataset.cpp',
        '../src/rdf/pattern.cpp',
        '../src/storage/stores/mem_rdf_tree2.cpp',
        '../src/storage/stores/trees/mem_node2.cpp',
        '../src/rdf/term/rdf_term.cpp',
        '../src/storage/dictionary/trie_dictionary.cpp',
        '../src/storage/dictionary/mem_dict_trie.cpp',
        '../src/storage/dictionary/dict_map.cpp',
        '../src/storage/stores/trees/mem_bidirectional_node.cpp',
        '../src/rdf/static_ordering.cpp',
        '../src/util/thread_pool.cpp'
]

src_versioning = [
        '../src/rdf/parser/ntparser.cpp',
        '../src/rdf/mem_ver_dataset.cpp',
        '../src/rdf/pattern.cpp',
        '../src/storage/stores/mem_rdf_tree2.cpp',
        '../src/storage/stores/trees/mem_node2.cpp',
        '../src/rdf/term/rdf_term.cpp',
        '../src/storage/dictionary/trie_dictionary.cpp',
        '../src/storage/dictionary/dict_map.cpp',
        '../src/storage/stores/trees/mem_bidirectional_node.cpp',
        '../src/rdf/static_ordering.cpp',
        '../src/util/thread_pool.cpp'
]

src_tuples = [
        '../src/rdf/parser/fivetuples_parser.cpp',
        '../src/rdf/mem_quad_ver_dataset.cpp',
        '../src/rdf/pattern.cpp',
        '../src/storage/stores/mem_rdf_tree2.cpp',
        '../src/storage/stores/trees/mem_node2.cpp',
        '../src/rdf/term/rdf_term.cpp',
        '../src/storage/dictionary/trie_dictionary.cpp',
        '../src/storage/stores/trees/mem_bidirectional_node.cpp',
        '../src/rdf/static_ordering.cpp'
]

# OpenMP dependency
ompdep = dependency('openmp')

thdep = dependency('threads')

src_triple = ['load_triple.cpp'] + src_common
src_triple_qr = ['load_and_query_triple.cpp'] + src_common
src_ver = ['load_and_query_ver_triple.cpp'] + src_versioning
src_qver = ['load_query_ver_quads.cpp'] + src_tuples

executable('load_triple', sources : src_triple, include_directories : incdir, dependencies : ompdep)
executable('load_triple_qr', sources : src_triple_qr, include_directories : incdir, dependencies : [ompdep, thdep])
executable('versioning_qr', sources : src_ver, include_directories : incdir, dependencies : ompdep)
executable('tuples_qr', sources : src_qver, include_directories : incdir, dependencies : ompdep)
