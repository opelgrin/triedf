Each experiment should output in the following format:
Single text file named after the solution (eg. "jena.txt")

Each line in the text file contain an integer, and the final line contain a float. 
- 0 to n-2 int: runtime in nanoseconds of each query
- n-1 int: total runtime in milliseconds of all queries
- n int: loading time in milliseconds 
- float: total runtime in seconds
