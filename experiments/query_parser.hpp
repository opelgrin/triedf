#pragma once

#include <fstream>
#include <vector>
#include <string>
#include <iostream>

#include "rdf/pattern.hpp"
#include "util/util.hpp"


inline rdftrie::Pattern parse_line_triple(std::string line) {
    std::vector<std::string> tokens;
    rdftrie::split(line, "-&-", tokens);
    rdftrie::tuple_order to = rdftrie::gen_pref_order(tokens);
    rdftrie::Pattern p(tokens, to);
    return p;
}


inline void parse_file(std::string file_path, std::vector<rdftrie::Pattern>& queries) {
    std::ifstream f(file_path, std::ifstream::in);
    if (f.fail()) {
        std::cerr << "ERROR: impossible to read the query file \"" << file_path << "\"" << std::endl;
        return;
    }

    std::string l;
    while (std::getline(f, l)) {
        if (l.empty()) {
            continue;
        }
        rdftrie::Pattern p = parse_line_triple(l);
        queries.push_back(p);
    }

    f.close();
}


inline std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t> parse_line_triple_ver(std::string line) {
    std::vector<std::string> tokens;
    rdftrie::split(line, "-&-", tokens);
    rdftrie::tuple_order to;
    if(tokens.size() == 3) {
        to = rdftrie::gen_pref_order(tokens);
        rdftrie::Pattern p(tokens, to);
        return std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t>(p, 0, 0);
    } else if (tokens.size() > 3) {
        std::vector<std::string> tmp;
        tmp.push_back(tokens[0]);
        tmp.push_back(tokens[1]);
        tmp.push_back(tokens[2]);
        to = rdftrie::gen_pref_order(tmp);
        rdftrie::Pattern p(tmp, to);
        uintptr_t v1 = std::stoull(tokens[3]);
        uintptr_t v2 = tokens.size() == 5 ? std::stoull(tokens[4]) : 0;
        return std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t>(p, v1, v2);
    }
    // default, should not happens
    std::cerr << "Error parsing line: \"" << line << "\" ... generating default." << std::endl;
    to = rdftrie::SPOV;
    rdftrie::Pattern p(tokens, to);
    return std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t>(p, 0, 0);
}


inline std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t> parse_line_quads_ver(std::string line) {
    std::vector<std::string> tokens;
    rdftrie::split(line, "-&-", tokens);
    std::vector<std::string> spo_tokens = {tokens[0], tokens[1], tokens[2]};
    rdftrie::tuple_order to = rdftrie::gen_pref_order(spo_tokens);
    spo_tokens.push_back(tokens.back());
    rdftrie::Pattern p(spo_tokens, to);
    if(tokens.size() == 5) {
        uintptr_t v1 = tokens[3] == "?" ? 0 : std::stoull(tokens[3]);
        return std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t>(p, v1, 0);
    } else if (tokens.size() > 5) {
        uintptr_t v1 = std::stoull(tokens[3]);
        uintptr_t v2 = tokens.size() == 6 ? std::stoull(tokens[4]) : 0;
        return std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t>(p, v1, v2);
    }
    // default, should not happens
    std::cerr << "Error parsing line: \"" << line << "\" ... generating default." << std::endl;
    to = rdftrie::SPORV;
    p = rdftrie::Pattern(spo_tokens, to);
    return std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t>(p, 0, 0);
}


inline void parse_file_ver(std::string file_path, std::vector<std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t>>& queries) {
    std::ifstream f(file_path, std::ifstream::in);
    if (f.fail()) {
        std::cerr << "ERROR: impossible to read the query file \"" << file_path << "\"" << std::endl;
        return;
    }

    std::string l;
    while (std::getline(f, l)) {
        if (l.empty()) {
            continue;
        }
        std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t> p = parse_line_triple_ver(l);
        queries.push_back(p);
    }

    f.close();
}


inline void parse_file_quad_ver(std::string file_path, std::vector<std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t>>& queries) {
    std::ifstream f(file_path, std::ifstream::in);
    if (f.fail()) {
        std::cerr << "ERROR: impossible to read the query file \"" << file_path << "\"" << std::endl;
        return;
    }

    std::string l;
    while (std::getline(f, l)) {
        if (l.empty()) {
            continue;
        }
        std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t> p = parse_line_quads_ver(l);
        queries.push_back(p);
    }

    f.close();
}



