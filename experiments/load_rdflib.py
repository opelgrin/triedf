import sys
import rdflib
import time


def make_graph(path: str):
    g = rdflib.Graph()
    g.parse(path, format=rdflib.util.guess_format(path))
    return g


def parse_queries(path: str):
    qr = []
    with open(path, 'r') as f:
        for l in f:
            ls = l.rstrip().split('-&-')
            qr.append(ls)
    return qr


def make_sparql(qr: list):
    sparql = []
    for q in qr:
        s = '?s' if q[0] == '?' else q[0]
        p = '?p' if q[1] == '?' else q[1]
        o = '?o' if q[2] == '?' else q[2]
        qs = f'SELECT * WHERE {{ {s} {p} {o} }}'
        sparql.append(qs)
    return sparql


def run_queries(graph: rdflib.Graph, queries: list):
    t = []
    for qr in queries:
        t1 = time.perf_counter_ns()
        res = graph.query(qr)
        t2 = time.perf_counter_ns()
        tt = t2 - t1
        t.append(tt)
        print(qr)
        print(f'Number of results: {len(res)} , runtime: {tt} nanoseconds.')
        print()
    return t


def main():
    if len(sys.argv) != 3:
        print('Missing arguments: <filename> <queries_file>')
        sys.exit()

    print('Loading file ' + sys.argv[1] + ' ...')
    tl1 = time.perf_counter_ns()
    g = make_graph(sys.argv[1])
    tl2 = time.perf_counter_ns()
    load_time = (tl2 - tl1) // 1000000
    print(f'Number of statements in graph: {len(g)}')

    print('Preparing queries...')
    qr = parse_queries(sys.argv[2])
    sparql = make_sparql(qr)
    print('Querying...')
    tqr1 = time.perf_counter_ns()
    times = run_queries(g, sparql)
    tqr2 = time.perf_counter_ns()
    query_time = (tqr2 - tqr1) // 1000000

    times.append(query_time)
    times.append(load_time)

    with open('rdflib.txt', 'w', encoding='utf-8') as f:
        for t in times:
            f.write(str(t) + '\n')
    # input('Press any to continue...')


main()
