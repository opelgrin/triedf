#include <chrono>
#include <fstream>
#include <iostream>

#include "rdf/ver_quad_dataset.hpp"
#include "query_parser.hpp"

#include "tclap/CmdLine.h"


void print_pattern(std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t> p) {
    for (auto v: std::get<0>(p).get_pattern()) {
        std::cout << v << " ";
    }
    std::cout << std::get<1>(p) << " " << std::get<2>(p) << std::endl;
}

void load_and_query(std::string file, std::string query_file) {
    std::string res = "";

    std::vector<rdftrie::tuple_order> orders = {rdftrie::POS, rdftrie::OSP};
    rdftrie::MemQuadVersionedDataset dataset(orders);

    auto start = std::chrono::high_resolution_clock::now();
    dataset.load_from_file(file);
    auto end = std::chrono::high_resolution_clock::now();
    auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(end-start);

    // Query
    std::cout << "Preparation of queries..." << std::endl;
    std::vector<std::tuple<rdftrie::Pattern, uintptr_t, uintptr_t>> queries;
    parse_file_quad_ver(query_file, queries);
    auto total_qr_start = std::chrono::high_resolution_clock::now();
    for (auto& qr: queries) {
        std::cout << "QUERY ";
        if (std::get<1>(qr) == 0) {
            if (std::get<2>(qr) == 0) {  // simple VQ query
                std::cout << "VQ: ";
                print_pattern(qr);
                std::set<std::vector<std::string>> results;
                auto start_qr = std::chrono::high_resolution_clock::now();
                dataset.query_vq(std::get<0>(qr), results);
                auto end_qr = std::chrono::high_resolution_clock::now();
                auto runtime_qr = std::chrono::duration_cast<std::chrono::nanoseconds>(end_qr-start_qr);
                std::cout << "Query results: " << results.size() << " , runtime: " << runtime_qr.count() << " nanoseconds." << std::endl;
                res += std::to_string(runtime_qr.count()) + "\n";
            }
        } else {  // VM or DM
            if (std::get<2>(qr) == 0) {  // VM
                std::cout << "VM: ";
                print_pattern(qr);
                std::set<std::vector<std::string>> results;
                auto start_qr = std::chrono::high_resolution_clock::now();
                dataset.query_vm(std::get<0>(qr), std::get<1>(qr), results);
                auto end_qr = std::chrono::high_resolution_clock::now();
                auto runtime_qr = std::chrono::duration_cast<std::chrono::nanoseconds>(end_qr-start_qr);
                std::cout << "Query results: " << results.size() << " , runtime: " << runtime_qr.count() << " nanoseconds." << std::endl;
                res += std::to_string(runtime_qr.count()) + "\n";
            } else {  // DM
                std::cout << "DM: ";
                print_pattern(qr);
                std::set<std::vector<std::string>> res_add;
                std::set<std::vector<std::string>> res_del;
                auto start_qr = std::chrono::high_resolution_clock::now();
                dataset.query_dm(std::get<0>(qr), std::get<1>(qr), std::get<2>(qr), res_add, res_del);
                auto end_qr = std::chrono::high_resolution_clock::now();
                auto runtime_qr = std::chrono::duration_cast<std::chrono::nanoseconds>(end_qr-start_qr);
                std::cout << "Query results: add(" << res_add.size() << ") del(" << res_del.size() << ") , runtime: " << runtime_qr.count() << " nanoseconds." << std::endl;
                res += std::to_string(runtime_qr.count()) + "\n";
            }
        }
    }
    auto total_qr_end = std::chrono::high_resolution_clock::now();
    auto total_qr_runtime = std::chrono::duration_cast<std::chrono::milliseconds>(total_qr_end-total_qr_start);
    std::cout << "Total query runtime: " << total_qr_runtime.count() << std::endl;
    res += std::to_string(runtime.count()) + "\n";

    std::ofstream outfile;
    outfile.open("rdftrie_quad_ver.txt", std::ios_base::app);
    outfile << res;
    outfile.close();
}


bool parse_arguments(int argc, char const *argv[],
		std::map<std::string, std::unique_ptr<TCLAP::ValueArg<std::string>>> &parsedArgs) {
    try {
    	TCLAP::CmdLine cmd("Utility to load and query an RDF dataset", '=', "0.1", true);

        // Define a value argument and add it to the command line.
        std::unique_ptr<TCLAP::ValueArg<std::string>> fileArg(new TCLAP::ValueArg<std::string>("f", "file", "Path to a file to load", true, "", "string"));
        cmd.add(*fileArg);

		std::unique_ptr<TCLAP::ValueArg<std::string>> queriesArg(new TCLAP::ValueArg<std::string>("q", "queries", "Queries file", true, "", "string"));

        cmd.add(*queriesArg);

        // Parse the args
        cmd.parse(argc, argv);
        parsedArgs.insert(std::make_pair("file", std::move(fileArg)));
        parsedArgs.insert(std::make_pair("queries", std::move(queriesArg)));

        return true;

    } catch (TCLAP::ArgException &e) {
    	std::cerr << "There was a problem when parsing the program arguments" << std::endl;
        std::cerr << "Error: " << e.error() << " for argument " << e.argId() << std::endl;
        return false;
    }
}


int main(int argc, char const *argv[])
{
    std::map<std::string, std::unique_ptr<TCLAP::ValueArg<std::string>>> parsedArgs;
    if (!parse_arguments(argc, argv, parsedArgs)) 
        return 1;
    
    load_and_query(parsedArgs["file"]->getValue(), parsedArgs["queries"]->getValue());
    return 0;
}

