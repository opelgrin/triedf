#include <iostream>

#include "util/thread_pool.hpp"

namespace rdftrie {

    ThreadPool::ThreadPool() {
        unsigned n_threads = std::thread::hardware_concurrency();
        worker_threads.reserve(n_threads);

        for(unsigned i=0; i<n_threads; i++) {
            worker_threads.emplace_back(std::bind(&ThreadPool::thread_main_loop, this));
        }
    }

    ThreadPool::ThreadPool(unsigned n_threads) {
        worker_threads.reserve(n_threads);

        for(unsigned i=0; i<n_threads; i++) {
            worker_threads.emplace_back(std::bind(&ThreadPool::thread_main_loop, this));
        }
        for (auto& th: worker_threads) {
            work_status[th.get_id()] = false;
        }
    }

    ThreadPool::~ThreadPool() {    
        {
            std::unique_lock<std::mutex> ul(lock);
            
            shutdown = true;
            ul.unlock();
            cond_var.notify_all();
        }
        
        for (auto& th : worker_threads) {
            th.join();
        }
    }

    void ThreadPool::submit_job(std::function<void(void)> job) {
        auto j = [job, this]() {
            // Set thread status to "working"
            std::unique_lock<std::mutex> ul(wlock);
            work_status[std::this_thread::get_id()] = true;
            ul.unlock();
            
            // work
            job();
            
            // Set thread status to "done working"
            ul.lock();
            work_status[std::this_thread::get_id()] = false;
            ul.unlock();
            wcv.notify_all();
        };

        std::unique_lock<std::mutex> ul(lock);

        jobs.emplace(std::move(j));
        cond_var.notify_one();
    }

    void ThreadPool::submit_jobs(std::vector<std::function<void(void)>> jobs_list) {
        for (auto& job : jobs_list) {
            submit_job(job);
        }
    }

    void ThreadPool::submit_jobs_and_wait(std::vector<std::function<void(void)>> jobs_list) {
        unsigned finished_jobs = 0;

        std::mutex jslock;
        std::condition_variable cv;
        
        for (auto& job : jobs_list) {
            
            auto f = [job, &finished_jobs, &jslock, &cv, this]() {
                // Set thread status to "working"
                std::unique_lock<std::mutex> wul(wlock);
                work_status[std::this_thread::get_id()] = true;
                wul.unlock();

                // do work
                job();

                // Set thread status to "done working"
                wul.lock();
                work_status[std::this_thread::get_id()] = false;
                wul.unlock();
                wcv.notify_all();

                // Notify for "job completed"
                std::unique_lock <std::mutex> ul(jslock);
                finished_jobs++;
                ul.unlock();
                cv.notify_all();
            };

            std::unique_lock <std::mutex> ul(lock);
            jobs.emplace(f);
            ul.unlock();
            cond_var.notify_one();
        }

        while(finished_jobs != jobs_list.size()) {
            std::unique_lock <std::mutex> ul(jslock);
            cv.wait(ul);
        }
    }

    bool ThreadPool::is_pool_busy() {
        {
            std::unique_lock <std::mutex> ul(lock);
            if (!jobs.empty())
                return true;
        }
        bool work = false;
        for (auto& ths: work_status) {
            work = ths.second || work;
            if (work)
                break;
        }
        return work;
    }

    void ThreadPool::wait_completion() {
        while (is_pool_busy()) {
            std::unique_lock<std::mutex> wul(wlock);
            wcv.wait(wul);
        }        
    }

    void ThreadPool::thread_main_loop() {
        std::function<void(void)> job;

        while(1) {

            {
                std::unique_lock<std::mutex> ul(lock);

                while (!shutdown && jobs.empty()) {
                    cond_var.wait(ul);
                }

                if (jobs.empty()) {
                    return;
                }

                job = std::move(jobs.front());
                jobs.pop();
            }

            //Execute the job
            job();
        }
    }

    size_t ThreadPool::get_num_workers() {
        return worker_threads.size();
    }

}