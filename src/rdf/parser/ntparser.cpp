#include <fstream>
#include <iostream>

#include "rdf/parser/ntparser.hpp"
#include "util/util.hpp"


namespace rdftrie {

const std::string NTParser::uriref = "<([^:]+:[^\\s\"<>]*)>";
// const std::string NTParser::literal_string = "\"([^\"\\\\]*(?:\\.[^\"\\\\]*)*)\"";
const std::string NTParser::literal_string = "\"([^\"]*(?:\\.[^\"\\\\]*)*)\"";
const std::string NTParser::literal_info = "(?:@([a-zA-Z]+(?:-[a-zA-Z0-9]+)*)|\\^\\^" + NTParser::uriref + ")?";
const std::string NTParser::literal = literal_string + literal_info;
const std::string NTParser::wspaces = "[ \t]+";
const std::string NTParser::node_id = "_:([A-Za-z0-9_:]([-A-Za-z0-9_:.]*[-A-Za-z0-9_:])?)";


NTParser::NTParser(): sink(nullptr) {
    this->uriref_re = std::regex(NTParser::uriref, std::regex::ECMAScript);
    this->literal_re = std::regex(NTParser::literal, std::regex::ECMAScript);
    this->wspaces_re = std::regex(NTParser::wspaces, std::regex::ECMAScript);
    this->node_id_re = std::regex(NTParser::node_id, std::regex::ECMAScript);
}

NTParser::NTParser(TripleSink* sink) {
    this->uriref_re = std::regex(NTParser::uriref, std::regex::ECMAScript);
    this->literal_re = std::regex(NTParser::literal, std::regex::ECMAScript);
    this->wspaces_re = std::regex(NTParser::wspaces, std::regex::ECMAScript);
    this->node_id_re = std::regex(NTParser::node_id, std::regex::ECMAScript);

    this->sink = sink;
}

void NTParser::parse(const std::string file_path) {
    std::ifstream f(file_path, std::ifstream::in);
    if (f.fail()) {
        std::cerr << "ERROR: impossible to read file \"" << file_path << "\"" << std::endl;
        return;
    }

    std::string l;
    while (std::getline(f, l)) {
        if (l.empty()) {
            continue;
        }
        this->current_line = l;
        this->parseline();
    }

    f.close();
}

void NTParser::parseline() {
    if(sink) {
        Term s = this->subject();
        this->eat_whitespaces();
        Term p = this->predicate();
        this->eat_whitespaces();
        Term o = this->object();
        this->sink->triple(s, p, o);
    } else {
        std::cerr << "No sink available for Triple parser..." << std::endl;
    }
}

void NTParser::parse_string(const std::string str, std::vector<std::vector<Term>>& triples) {
    std::vector<std::string> lines;
    split(str, "\n", lines);
    for (std::string l: lines) {
        Term s = this->subject(l);
        this->eat_whitespaces(l);
        Term p = this->predicate(l);
        this->eat_whitespaces(l);
        Term o = this->object(l);
        std::vector<Term> t = {s, p, o};
        triples.push_back(t);
    }
}

void NTParser::eat_whitespaces() {
    eat_whitespaces(current_line);
}

void NTParser::eat_whitespaces(std::string& line) {
    std::smatch ws_match;
    if (std::regex_search(line, ws_match, this->wspaces_re))
        line = std::string(line, std::distance(ws_match[0].first, ws_match[0].second));
}

Term NTParser::subject() {
    return subject(current_line);
}

Term NTParser::subject(std::string& line) {
    std::smatch s_match;
    if (line[0] == '<') {
        if (std::regex_search(line, s_match, this->uriref_re)) {
            std::string s_str = s_match[0];
            line = std::string(line, std::distance(s_match[0].first, s_match[0].second));
            return Term(s_str, rdftrie::URI, rdftrie::SUBJECT);
        } else {
            std::cout << "NTParser::subject => can't parse URI in current line: " << line << std::endl;
            return Term("", rdftrie::ERROR, rdftrie::SUBJECT);
        }
    } else if (line[0] == '_') {
        if (std::regex_search(line, s_match, this->node_id_re)) {
            std::string s_str = s_match[0];
            line = std::string(line, std::distance(s_match[0].first, s_match[0].second));
            return Term(s_str, rdftrie::BLANK_NODE, rdftrie::SUBJECT);
        } else {
            std::cout << "NTParser::subject => can't parse BLANK_NODE in current line: " << line << std::endl;
            return Term("", rdftrie::ERROR, rdftrie::SUBJECT);
        }
    } 
    std::cout << "NTParser::subject => can't parse ANY subject in current line: " << line << std::endl;
    return Term("", rdftrie::ERROR, rdftrie::SUBJECT);
}

Term NTParser::predicate() {
    return predicate(current_line);
}

Term NTParser::predicate(std::string& line) {
    std::smatch s_match;
    if (line[0] == '<') {
        if (std::regex_search(line, s_match, this->uriref_re)) {
            std::string p_str = s_match[0];
            line = std::string(line, std::distance(s_match[0].first, s_match[0].second));
            return Term(p_str, rdftrie::URI, rdftrie::PREDICATE);
        }
    }
    std::cout << "NTParser::predicate => can't parse URI in current line: " << line << std::endl;
    return Term("", rdftrie::ERROR, rdftrie::PREDICATE);
}

Term NTParser::object() {
    return object(current_line);
}

Term NTParser::object(std::string& line) {
    std::smatch s_match;
    if (line[0] == '<') {
        if (std::regex_search(line, s_match, this->uriref_re)) {
            std::string o_str = s_match[0];
            return Term(o_str, rdftrie::URI, rdftrie::OBJECT);
        } else {
            std::cout << "NTParser::object => can't parse URI in current line: " << line << std::endl;
            return Term("", rdftrie::ERROR, rdftrie::OBJECT);
        }
    } else if (line[0] == '_') {
        if (std::regex_search(line, s_match, this->node_id_re)) {
            std::string o_str = s_match[0];
            return Term(o_str, rdftrie::BLANK_NODE, rdftrie::OBJECT);
        } else {
            std::cout << "NTParser::object => can't parse BLANK_NODE in current line: " << line << std::endl;
            return Term("", rdftrie::ERROR, rdftrie::OBJECT);
        }
    } else if (line[0] == '"') {
        // TODO: Seperate string from language tag and/or type
        // Maybe regex sub-matches would do the trick ?
        if (std::regex_search(line, s_match, this->literal_re)) {
            std::string o_str = s_match[0];
            return Term(o_str, rdftrie::LITERAL, rdftrie::OBJECT);
        } else {
            std::cout << "NTParser::object => can't parse LITERAL in current line: " << line << std::endl;
            return Term("", rdftrie::ERROR, rdftrie::OBJECT);
        }
    }
    std::cout << "NTParser::object => can't parse ANY object in current line: " << line << std::endl;
    return Term("", rdftrie::ERROR, rdftrie::OBJECT);
}


};