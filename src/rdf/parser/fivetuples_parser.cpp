#include <fstream>
#include <iostream>

#include "rdf/parser/fivetuples_parser.hpp"
#include "util/util.hpp"


namespace rdftrie {

    const std::string FiveTupleParser::__string = ".[^ \t]+";
    const std::string FiveTupleParser::__wspaces = "[ \t]+";
    const std::string FiveTupleParser::__version = "[0-9]+";

    FiveTupleParser::FiveTupleParser(): __sink(nullptr) {
        __string_re = std::regex(FiveTupleParser::__string, std::regex::ECMAScript);
        __wspaces_re = std::regex(FiveTupleParser::__wspaces, std::regex::ECMAScript);
        __version_re = std::regex(FiveTupleParser::__version, std::regex::ECMAScript);
    }

    FiveTupleParser::FiveTupleParser(FiveSink* sink) {
        __string_re = std::regex(FiveTupleParser::__string, std::regex::ECMAScript);
        __wspaces_re = std::regex(FiveTupleParser::__wspaces, std::regex::ECMAScript);
        __version_re = std::regex(FiveTupleParser::__version, std::regex::ECMAScript);

        __sink = sink;
    }

    void FiveTupleParser::parse(const std::string file_path) {
        std::ifstream f(file_path, std::ifstream::in);
        if (f.fail()) {
            std::cerr << "ERROR: impossible to read file \"" << file_path << "\"" << std::endl;
            return;
        }

        std::string l;
        while (std::getline(f, l)) {
            if (l.empty()) {
                continue;
            }
            __current_line = l;
            parseline();
        }

        f.close();
    }

    void FiveTupleParser::parseline() {
        if(__sink) {
            Term s = string(SUBJECT);
            eat_whitespaces();
            Term p = string(PREDICATE);
            eat_whitespaces();
            Term o = string(OBJECT);
            eat_whitespaces();
            Term v = version();
            eat_whitespaces();
            Term src = string(PROV);
            __sink->tuple(s, p, o, v, src);
        } else {
            std::cerr << "No sink available for Triple parser..." << std::endl;
        }
    }

    void FiveTupleParser::parse_string(const std::string str, std::vector<std::vector<Term>>& tuples) {
        std::vector<std::string> lines;
        split(str, "\n", lines);
        for (std::string l: lines) {
            Term s = string(l, SUBJECT);
            eat_whitespaces(l);
            Term p = string(l, PREDICATE);
            eat_whitespaces(l);
            Term o = string(l, OBJECT);
            eat_whitespaces(l);
            Term v = version(l);
            eat_whitespaces(l);
            Term src = string(l, PROV);
            std::vector<Term> t = {s, p, o, v, src};
            tuples.push_back(t);
        }
    }

    void FiveTupleParser::eat_whitespaces() {
        eat_whitespaces(__current_line);
    }

    void FiveTupleParser::eat_whitespaces(std::string& line) {
        std::smatch ws_match;
        if (std::regex_search(line, ws_match, __wspaces_re))
            line = std::string(line, std::distance(ws_match[0].first, ws_match[0].second));
    }

    Term FiveTupleParser::string(rdf_term_type term_type) {
        return string(__current_line, term_type);
    }

    Term FiveTupleParser::string(std::string& line, rdf_term_type term_type) {
        std::smatch s_match;
        if (std::regex_search(line, s_match, __string_re)) {
            std::string s_str = s_match[0];
            line = std::string(line, std::distance(s_match[0].first, s_match[0].second));
            return Term(s_str, LITERAL, term_type);
        } else {
            std::cout << "FiveTupleParser::string => can't parse string in current line: " << line << std::endl;
            return Term("", rdftrie::ERROR, rdftrie::SUBJECT);
        }
    }

    Term FiveTupleParser::version() {
        return version(__current_line);
    }

    Term FiveTupleParser::version(std::string& line) {
        std::smatch s_match;
        if (std::regex_search(line, s_match, __version_re)) {
            std::string s_str = s_match[0];
            line = std::string(line, std::distance(s_match[0].first, s_match[0].second));
            return Term(s_str, VERSION_ID, VERSION);
        } else {
            std::cout << "FiveTupleParser::version => can't parse version_id in current line: " << line << std::endl;
            return Term("", rdftrie::ERROR, rdftrie::VERSION);
        }
    }


    // -----------------------------------------------

    FiveTupleParserFast::FiveTupleParserFast(): __sep("\t") {}

    FiveTupleParserFast::FiveTupleParserFast(FiveSink* sink): __sep("\t") {
        __sink = sink;
    }

    void FiveTupleParserFast::parse(const std::string file_path) {
        std::ifstream f(file_path, std::ifstream::in);
        if (f.fail()) {
            std::cerr << "ERROR: impossible to read file \"" << file_path << "\"" << std::endl;
            return;
        }

        std::string l;
        while (std::getline(f, l)) {
            if (l.empty()) {
                continue;
            }
            __current_line = l;
            parseline();
        }

        f.close();
    }

    void FiveTupleParserFast::parseline() {
        if(__sink) {
            std::vector<std::string> tokens;
            split(__current_line, __sep, tokens);
            Term s(tokens[0], LITERAL, SUBJECT);
            Term p(tokens[1], LITERAL, PREDICATE);
            Term o(tokens[2], LITERAL, OBJECT);
            Term v(tokens[3], VERSION_ID, VERSION);
            Term q(tokens[4], LITERAL, PROV);
            __sink->tuple(s, p, o, v, q);
        } else {
            std::cerr << "No sink available for Triple parser..." << std::endl;
        }
    }

    void FiveTupleParserFast::parse_string(const std::string str, std::vector<std::vector<Term>>& tuples) {
        std::vector<std::string> lines;
        split(str, "\n", lines);
        for (std::string l: lines) {
            std::vector<std::string> tokens;
            split(l, __sep, tokens);
            Term s(tokens[0], LITERAL, SUBJECT);
            Term p(tokens[1], LITERAL, PREDICATE);
            Term o(tokens[2], LITERAL, OBJECT);
            Term v(tokens[3], VERSION_ID, VERSION);
            Term q(tokens[4], LITERAL, PROV);
            std::vector<Term> t = {s, p, o, v, q};
            tuples.push_back(t);
        }
    }
}