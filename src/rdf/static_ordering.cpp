#include "rdf/static_ordering.hpp"

namespace rdftrie {

    // Assume that the original tuple order is SPO
	std::vector<uint64_t> reorder_triple(const std::vector<uint64_t>& t1, tuple_order new_order) {
		if (new_order == SPO) {
			std::vector<uint64_t> t(t1);
			return t;
		}
		std::vector<uint64_t> t;
		if (new_order == SOP) {
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
		} else if (new_order == PSO) {
			t.push_back(t1[1]);
			t.push_back(t1[0]);
			t.push_back(t1[2]);
		} else if (new_order == POS) {
			t.push_back(t1[1]);
			t.push_back(t1[2]);
			t.push_back(t1[0]);			
		} else if (new_order == OSP) {
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[1]);
		} else if (new_order == OPS) {
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[0]);
		}
		return t;
	}

	std::vector<uint64_t> reorder_to_spo(const std::vector<uint64_t>& t1, tuple_order old_order) {
		if (old_order == SPO) {
			std::vector<uint64_t> t(t1);
			return t;
		}
		std::vector<uint64_t> t;
		if (old_order == SOP) {
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
		} else if (old_order == PSO) {
			t.push_back(t1[1]);
			t.push_back(t1[0]);
			t.push_back(t1[2]);
		} else if (old_order == POS) {
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[1]);			
		} else if (old_order == OSP) {
			t.push_back(t1[1]);
			t.push_back(t1[2]);
			t.push_back(t1[0]);
		} else if (old_order == OPS) {
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[0]);
		}
		return t;
	}

	std::vector<uintptr_t> reorder_to_spov(const std::vector<uintptr_t>& t1, tuple_order old_order) {
		if (old_order == SPOV) {
			std::vector<uintptr_t> t(t1);
			return t;
		}
		std::vector<uintptr_t> t;
		if (old_order == SOPV) {
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
		} else if (old_order == PSOV) {
			t.push_back(t1[1]);
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[3]);
		} else if (old_order == POSV) {
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);	
		} else if (old_order == OSPV) {
			t.push_back(t1[1]);
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[3]);
		} else if (old_order == OPSV) {
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[0]);
			t.push_back(t1[3]);
		} else if (old_order == SOPV) {
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
		} else if (old_order == VSPO) {
			t.push_back(t1[1]);
			t.push_back(t1[2]);
			t.push_back(t1[3]);
			t.push_back(t1[0]);
		} else if (old_order == VSOP){
			t.push_back(t1[1]);
			t.push_back(t1[3]);
			t.push_back(t1[2]);
			t.push_back(t1[0]);
		} else if (old_order == VPSO) {
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
			t.push_back(t1[0]);
		} else if (old_order == VPOS) {
			t.push_back(t1[3]);
			t.push_back(t1[1]);
			t.push_back(t1[2]);
			t.push_back(t1[0]);		
		} else if (old_order == VOSP) {
			t.push_back(t1[2]);
			t.push_back(t1[3]);
			t.push_back(t1[1]);
			t.push_back(t1[0]);
		} else if (old_order == VOPS) {
			t.push_back(t1[3]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[0]);
		}
		return t;
	}

	std::vector<uintptr_t> reorder_vertriple(const std::vector<uintptr_t>& t1, tuple_order new_order) {
		if (new_order == SPOV) {
			std::vector<uintptr_t> t(t1);
			return t;
		}
		std::vector<uintptr_t> t;
		if (new_order == SOPV) {
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
		} else if (new_order == PSOV) {
			t.push_back(t1[1]);
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[3]);
		} else if (new_order == POSV) {
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);	
		} else if (new_order == OSPV) {
			t.push_back(t1[1]);
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[3]);
		} else if (new_order == OPSV) {
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[0]);
			t.push_back(t1[3]);
		} else if (new_order == SOPV) {
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
		} else if (new_order == VSPO) {
			t.push_back(t1[3]);
			t.push_back(t1[0]);
			t.push_back(t1[1]);			
			t.push_back(t1[2]);
		} else if (new_order == VSOP){
			t.push_back(t1[3]);
			t.push_back(t1[0]);			
			t.push_back(t1[2]);
			t.push_back(t1[1]);
		} else if (new_order == VPSO) {
			t.push_back(t1[3]);
			t.push_back(t1[1]);
			t.push_back(t1[0]);			
			t.push_back(t1[2]);
		} else if (new_order == VPOS) {
			t.push_back(t1[3]);
			t.push_back(t1[1]);
			t.push_back(t1[2]);
			t.push_back(t1[0]);		
		} else if (new_order == VOSP) {
			t.push_back(t1[3]);
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[1]);
		} else if (new_order == VOPS) {
			t.push_back(t1[3]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[0]);
		}
		return t;
	}

	tuple_order gen_pref_order(const std::vector<std::string>& pattern) {
		if (pattern[0] == "?" && pattern[1] == "?" && pattern[2] == "?") {
			return SPO;
		}
		if (pattern[0] != "?") {
			if (pattern[1] == "?") {
				return SOP;
			} else {
				return SPO;
			}
		} else if (pattern[1] != "?") {
			if (pattern[0] == "?") {
				return POS;
			} else {
				return PSO;
			}
		} else if (pattern[2] != "?") {
			if (pattern[0] == "?") {
				return OPS;
			} else {
				return OSP;
			}
		}
		return SPO;
	}

	std::vector<uintptr_t> reorder_verquad(const std::vector<uintptr_t>& t1, tuple_order new_order) {
		if (new_order == SPORV) {
			std::vector<uintptr_t> t(t1);
			return t;
		}
		std::vector<uintptr_t> t;
		if (new_order == SOPRV) {
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		} else if (new_order == PSORV) {
			t.push_back(t1[1]);
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		} else if (new_order == POSRV) {
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		} else if (new_order == OSPRV) {
			t.push_back(t1[1]);
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		} else if (new_order == OPSRV) {
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[0]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		} else if (new_order == SOPRV) {
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		}
		return t;
	}


	std::vector<uintptr_t> reorder_to_sporv(const std::vector<uintptr_t>& t1, tuple_order old_order) {
		if (old_order == SPORV) {
			std::vector<uintptr_t> t(t1);
			return t;
		}
		std::vector<uintptr_t> t;
		if (old_order == SOPRV) {
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		} else if (old_order == PSORV) {
			t.push_back(t1[1]);
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		} else if (old_order == POSRV) {
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		} else if (old_order == OSPRV) {
			t.push_back(t1[1]);
			t.push_back(t1[2]);
			t.push_back(t1[0]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		} else if (old_order == OPSRV) {
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[0]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		} else if (old_order == SOPRV) {
			t.push_back(t1[0]);
			t.push_back(t1[2]);
			t.push_back(t1[1]);
			t.push_back(t1[3]);
			t.push_back(t1[4]);
		}
		return t;
	}
}
