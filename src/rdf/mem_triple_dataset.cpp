#include <omp.h>

#include <chrono>
#include <iostream>
#include <fstream>

#include "rdf/dataset.hpp"

namespace rdftrie {

    MemTripleDataset::MemTripleDataset(): __use_mt(true) {
        __trees.emplace(std::make_pair(SPO, MemRDFTree2()));       
    }

    MemTripleDataset::MemTripleDataset(std::vector<tuple_order> add_orders): __use_mt(true) {
        __trees.emplace(std::make_pair(SPO, MemRDFTree2()));
        for (tuple_order to: add_orders) {
            __trees.emplace(std::make_pair(to, MemRDFTree2()));
        }
    }

    void MemTripleDataset::add_tuple(std::vector<Term>& tuple) {
        __insert_tuple(tuple);
    }

    // Need to be replaced by a proper bulk insertion
    void MemTripleDataset::add_tuples(std::vector<std::vector<Term>>& tuples) {
        for (auto t: tuples) {
            add_tuple(t);
        }
    }

    bool MemTripleDataset::__pattern_to_int(Pattern& p, std::vector<uint64_t>& p2) {
        for (auto s: p.get_pattern()) {
            uint64_t id = 0UL;
            if (s != "?") {
                Term term(s, UNKNOWN, QUERY);
                id = __dict.string_to_id(term);
                if (id == 0UL) {
                    return false;
                }
            }
            p2.push_back(id);          
        }
        return true;
    }

    void MemTripleDataset::__triple_to_string(std::vector<uint64_t>& triple_int, std::vector<std::string>& triple_str) {
        for (uint64_t t: triple_int) {
            triple_str.emplace_back(__dict.id_to_string(t));
        }
    }

    inline void MemTripleDataset::__unserialize_triples(std::set<std::vector<uint64_t>>& triples_int, std::set<std::vector<std::string>>& triples_str, tuple_order query_order) {
        for (auto& v: triples_int) {
            std::vector<std::string> triple_str;
            std::vector<uint64_t> tmp = reorder_to_spo(v, query_order);
            __triple_to_string(tmp, triple_str);
            triples_str.insert(triple_str);
        }
    }

    inline void MemTripleDataset::__unserialize_triples_parallel(std::set<std::vector<uint64_t>>& triples_int, std::set<std::vector<std::string>>& triples_str, tuple_order query_order) {
        std::vector<std::vector<uint64_t>> tmp;
        tmp.insert(tmp.begin(), triples_int.begin(), triples_int.end());
        #pragma omp parallel for
        for (auto& v: tmp) {
            std::vector<std::string> triple_str;
            std::vector<uint64_t> tmp = reorder_to_spo(v, query_order);
            __triple_to_string(tmp, triple_str);
            #pragma omp critical(push_results)
            {
                triples_str.insert(triple_str);
            }
        }
    }

    // inline tuple_order MemTripleDataset::get_index(tuple_order pref) {
    //     if (__trees.find(pref) != __trees.end()) {
    //         return pref;
    //     }

    //     if (pref == PSO) {
    //         if (__trees.find(POS) != __trees.end()) {
    //             return POS;
    //         }
    //     } else if (pref == POS) {
    //         if (__trees.find(PSO) != __trees.end()) {
    //             return PSO;
    //         } else if(__trees.find(OPS) != __trees.end()) {
    //             return OPS;
    //         }
    //     } else if (pref == OSP) {
    //         if (__trees.find(OPS) != __trees.end()) {
    //             return OPS;
    //         } else if (__trees.find(SOP) != __trees.end()) {
    //             return SOP;
    //         }
    //     } else if (pref == OPS) {
    //         if (__trees.find(OSP) != __trees.end()) {
    //             return OSP;
    //         } else if (__trees.find(POS) != __trees.end()) {
    //             return POS;
    //         }
    //     }
    //     return SPO;
    // }

    tuple_order MemTripleDataset::get_index(Pattern& p) {
        std::vector<std::string> pattern = p.get_pattern();
        if (pattern[0] == "?" && pattern[1] == "?" && pattern[2] == "?") {
			return SPO;
		}
		if (pattern[0] != "?") {  // bound S
			if (pattern[1] == "?") { // unbound P
                if (__trees.find(SOP) != __trees.end()) {
                    return SOP;
                } else if(__trees.find(SPO) != __trees.end()) {
                    return SPO;
                }
			}
		} else if (pattern[1] != "?") { // bound P
			if (pattern[0] == "?") { // unbound S
                if (__trees.find(PSO) != __trees.end()) {
				    return PSO;
                } else if (__trees.find(POS) != __trees.end()) {
                    return POS;
                } else if (__trees.find(OPS) != __trees.end()) {
                    return OPS;
                }
			} else { // bound S
                if (__trees.find(PSO) != __trees.end()) {
				    return PSO;
                } else if (__trees.find(POS) != __trees.end()) {
				    return POS;
				}
			}
		} else if (pattern[2] != "?") { // bound O
			if (pattern[0] == "?") { // unbound S
                if (__trees.find(OPS) != __trees.end()) {
				    return OPS;
                } else if (__trees.find(OSP) != __trees.end()) {
				    return OSP;
				} else if (__trees.find(POS) != __trees.end()) {
                    return POS;
                }
			} else {
                if (__trees.find(OSP) != __trees.end()) {
				    return OSP;
                } else if (__trees.find(OPS) != __trees.end()) {
				    return OPS;
				} else if (__trees.find(SOP) != __trees.end()) {
                    return SOP;
                }
			}
		}
		return SPO;
    }


    void MemTripleDataset::query(Pattern& pattern, std::set<std::vector<std::string>>& results) {
        std::set<std::vector<uint64_t>> results_int;
        std::vector<uint64_t> p_int;
        bool st = __pattern_to_int(pattern, p_int);
        if (st) {
            // tuple_order query_order = get_index(pattern.get_preferred_order());
            tuple_order query_order = get_index(pattern);
            std::vector<uint64_t> p_tmp = reorder_triple(p_int, query_order);            
            __trees[query_order].query(p_tmp, results_int);
            if (results_int.size() > 10000) {
                __unserialize_triples_parallel(results_int, results, query_order);
            } else {
                __unserialize_triples(results_int, results, query_order);
            }
        }        
    }

    uint64_t MemTripleDataset::timed_query(Pattern& pattern) {
        auto start_qr = std::chrono::high_resolution_clock::now();
        std::set<std::vector<uint64_t>> results_int;
        std::vector<uint64_t> p_int;
        __pattern_to_int(pattern, p_int);
        tuple_order query_order = get_index(pattern);
        std::vector<uint64_t> p_tmp = reorder_triple(p_int, query_order);            
        __trees[query_order].query(p_tmp, results_int);
        auto end_qr = std::chrono::high_resolution_clock::now();
        auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end_qr-start_qr).count();
        return time;
    }

    void MemTripleDataset::__insert_tuple(std::vector<Term>& tuple) {
        // Pass strings to Dictionary and get IDs
        std::vector<uint64_t> int_tuple;
        for (auto& t: tuple) {
            uint64_t id = __dict.string_to_id(t);
            int_tuple.push_back(id);
        }
        for (auto& tree: __trees) {
            std::vector<uint64_t> triple = reorder_triple(int_tuple, tree.first);
            tree.second.put(triple);
        }
    }

    void MemTripleDataset::__parse_and_insert(std::string str) {
        std::vector<std::vector<Term>> triples;
        __ntparser.parse_string(str, triples);
        for (auto& t: triples) {
            __insert_tuple(t);
        }
    }

    // void MemTripleDataset::__bulk_parse_and_insert(std::string str){
        
    // }

    void MemTripleDataset::load_from_file(const std::string filename) {
        std::ifstream f(filename, std::ifstream::in);
        if (f.fail()) {
            std::cerr << "ERROR: impossible to read file \"" << filename << "\"" << std::endl;
            return;
        }
        #pragma omp parallel num_threads(2)
        {
            #pragma omp single
            {
                uint64_t ntriples = 0;
                std::string buffer;
                unsigned buffer_line_count = 50000;

                unsigned lc = 0;
                std::string l;
                while (std::getline(f, l)) {
                    if (l.empty()) {
                        continue;
                    }
                    buffer += l + "\n";
                    if (lc == buffer_line_count) {
                        #pragma omp task
                        {
                            __parse_and_insert(buffer);
                        }
                        buffer.clear();
                        lc = 0;
                    }
                    lc++;
                    ntriples++;
                    if (ntriples % 100000 == 0) {
                        std::cout << "\rProcessed " << ntriples << " triples..." << std::flush;
                    }
                }
                #pragma omp task
                {
                    __parse_and_insert(buffer);
                }
            }
        }
        f.close();
    }

}