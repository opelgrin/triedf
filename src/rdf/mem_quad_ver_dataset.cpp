// #include <omp.h>
#include <algorithm>
#include <iostream>
#include <fstream>

#include "rdf/ver_quad_dataset.hpp"


namespace rdftrie {

    MemQuadVersionedDataset::MemQuadVersionedDataset() {
        __trees.emplace(std::make_pair(SPORV, MemRDFTree2()));
    }

    MemQuadVersionedDataset::MemQuadVersionedDataset(std::vector<tuple_order> add_orders): __current_vid(1) {
        __trees.emplace(std::make_pair(SPORV, MemRDFTree2()));
        for (tuple_order to: add_orders) {
            __trees.emplace(std::make_pair(to_xxxrv(to), MemRDFTree2()));
        }
    }

    bool MemQuadVersionedDataset::__pattern_to_int(Pattern& p, std::vector<uintptr_t>& p2) {
        for (auto s: p.get_pattern()) {
            uintptr_t id = 0;
            
            if (s != "?") {
                Term term(s, UNKNOWN, QUERY);
                id = __dict.string_to_id(term);
                if (id == 0) {
                    return false;
                }
            }
            p2.push_back(id);
        }
        return true;
    }

    void MemQuadVersionedDataset::__tuple_to_string(std::vector<uintptr_t>& tuple_int, std::vector<std::string>& tuple_str) {
        for (size_t i=0; i<tuple_int.size()-1; i++) {
            tuple_str.push_back(__dict.id_to_string(tuple_int[i]));
        }
        tuple_str.push_back(std::to_string(tuple_int.back()));
    }

    void MemQuadVersionedDataset::__unserialize_tuples(std::set<std::vector<uintptr_t>>& tuples_int, std::set<std::vector<std::string>>& tuples_str, tuple_order query_order) {
        for (auto& v: tuples_int) {
            std::vector<std::string> tuple_str;
            std::vector<uintptr_t> tmp = reorder_to_sporv(v, query_order);
            __tuple_to_string(tmp, tuple_str);
            tuples_str.insert(tuple_str);
        }
    }

    void MemQuadVersionedDataset::__unserialize_tuples_parallel(std::set<std::vector<uintptr_t>>& tuples_int, std::set<std::vector<std::string>>& tuples_str, tuple_order query_order) {
        #pragma omp parallel
        {
            #pragma omp single
            {  
                for (auto& v: tuples_int) {
                    #pragma omp task
                    {
                        std::vector<std::string> tuple_str;
                        std::vector<uintptr_t> tmp = reorder_to_sporv(v, query_order);
                        __tuple_to_string(tmp, tuple_str);
                        #pragma omp critical(push_results)
                        {
                            tuples_str.insert(tuple_str);
                        }
                    }
                }
            }            
        }
    }

    void MemQuadVersionedDataset::__compute_delta(std::set<std::vector<uintptr_t>>& r1, std::set<std::vector<uintptr_t>>& r2, std::set<std::vector<uintptr_t>>& added, std::set<std::vector<uintptr_t>>& deleted) {
        std::set<std::vector<uintptr_t>> s1;
        std::set<std::vector<uintptr_t>> s2;
        #pragma omp parallel sections num_threads(2)
        {   
            #pragma omp section
            {
                for (auto& v: r1) {
                    std::vector<uintptr_t> t(v);
                    t.pop_back();
                    s1.insert(t);
                }
            }
            #pragma omp section
            {
                for (auto& v: r2) {
                    std::vector<uintptr_t> t(v);
                    t.pop_back();
                    s2.insert(t);
                }
            }            
        }
        #pragma omp parallel sections num_threads(2)
        {
            #pragma omp section
            {
                std::vector<std::vector<uintptr_t>> tmp;
                std::set_difference(s1.begin(),  s1.end(), s2.begin(), s2.end(), std::back_inserter(tmp));
                deleted.insert(tmp.begin(), tmp.end());
            }
            #pragma omp section
            {
                std::vector<std::vector<uintptr_t>> tmp;
                std::set_difference(s2.begin(), s2.end(), s1.begin(), s1.end(), std::back_inserter(tmp));
                added.insert(tmp.begin(), tmp.end());
            }
        }
    }


    inline tuple_order MemQuadVersionedDataset::to_xxxrv(tuple_order order) {
        switch (order) {
        case SPO:
            return SPORV;
        case SOP:
            return SOPRV;
        case PSO:
            return PSORV;
        case POS:
            return POSRV;
        case OSP:
            return OSPRV;
        case OPS:
            return OPSRV;
        default:
            return SPORV;
        }
    }

    inline tuple_order MemQuadVersionedDataset::get_index_dm(const std::vector<std::string>& pattern) {
        return get_index_vq(pattern);
    }

    inline tuple_order MemQuadVersionedDataset::get_index_vm(const std::vector<std::string>& pattern) {
        return get_index_vq(pattern);
    }

    inline tuple_order MemQuadVersionedDataset::get_index_vq(const std::vector<std::string>& pattern) {
        if (pattern[0] == "?" && pattern[1] == "?" && pattern[2] == "?") {
			return SPORV;
		}
		if (pattern[0] != "?") {  // bound S
			if (pattern[1] == "?") { // unbound P
                if (__trees.find(SOPRV) != __trees.end()) {
                    return SOPRV;
                } else if(__trees.find(OSPRV) != __trees.end()) {
                    return OSPRV;
                }
			}
		} else if (pattern[1] != "?") { // bound P
			if (pattern[0] == "?") { // unbound S
                if (__trees.find(POSRV) != __trees.end()) {
				    return POSRV;
                } else if (__trees.find(PSORV) != __trees.end()) {
                    return PSORV;
                } else if (__trees.find(OPSRV) != __trees.end()) {
                    return OPSRV;
                }
			} else { // bound S
                if (__trees.find(PSORV) != __trees.end()) {
				    return PSORV;
                } else if (__trees.find(POSRV) != __trees.end()) {
				    return POSRV;
				}
			}
		} else if (pattern[2] != "?") { // bound O
			if (pattern[0] == "?") { // unbound S
                if (__trees.find(OPSRV) != __trees.end()) {
				    return OPSRV;
                } else if (__trees.find(OSPRV) != __trees.end()) {
				    return OSPRV;
				} else if (__trees.find(POSRV) != __trees.end()) {
                    return POSRV;
                }
			} else {
                if (__trees.find(OSPRV) != __trees.end()) {
				    return OSPRV;
                } else if (__trees.find(OPSRV) != __trees.end()) {
				    return OPSRV;
				} else if (__trees.find(SOPRV) != __trees.end()) {
                    return SOPRV;
                }
			}
		}
		return SPORV;
    }

    void MemQuadVersionedDataset::__insert_tuple(std::vector<Term>& tuple) {
        // Pass strings to Dictionary and get ID
        std::vector<uintptr_t> int_tuple;
        uintptr_t vid;
        for (auto& t: tuple) {
            if (t.get_rdf_type() != VERSION_ID) {
                uintptr_t id = __dict.string_to_id(t);
                int_tuple.push_back(id); 
            } else {
                vid = std::stoull(t.get_string());
            }
        }
        int_tuple.push_back(vid);
        for (auto& tree: __trees) {
            std::vector<uintptr_t> t = reorder_verquad(int_tuple, tree.first);
            tree.second.put(t);
        }
        if (vid > __current_vid)
            __current_vid = vid;
    }

    void MemQuadVersionedDataset::add_tuple(std::vector<Term>& tuple) {
        __insert_tuple(tuple);
    }

    void MemQuadVersionedDataset::query_vm(Pattern& pattern, uintptr_t vid, std::set<std::vector<std::string>>& results) {
        std::set<std::vector<uintptr_t>> results_int;
        std::vector<uintptr_t> p_int;
        bool st = __pattern_to_int(pattern, p_int);
        if (st) {
            p_int.push_back(vid);
            tuple_order query_order = get_index_dm(pattern.get_pattern());
            std::vector<uintptr_t> p_tmp = reorder_verquad(p_int, query_order);            
            __trees[query_order].query(p_tmp, results_int);
            if (results_int.size() > 10000) {
                __unserialize_tuples_parallel(results_int, results, query_order);
            } else {
                __unserialize_tuples(results_int, results, query_order);
            }
        }
    }

    void MemQuadVersionedDataset::query(Pattern& pattern, std::set<std::vector<std::string>>& results) {
        query_vm(pattern, __current_vid, results);
    }

    void MemQuadVersionedDataset::query_vq(Pattern& pattern, std::set<std::vector<std::string>>& results) {
        std::set<std::vector<uintptr_t>> results_int;
        std::vector<uintptr_t> p_int;

        bool st = __pattern_to_int(pattern, p_int);
        if (st) {
            p_int.push_back(0);
            tuple_order query_order = get_index_vq(pattern.get_pattern());
            std::vector<uintptr_t> p_tmp = reorder_verquad(p_int, query_order);            
            __trees[query_order].query(p_tmp, results_int);
            if (results_int.size() > 10000) {
                __unserialize_tuples_parallel(results_int, results, query_order);
            } else {
                __unserialize_tuples(results_int, results, query_order);
            }
        }
    }

    void MemQuadVersionedDataset::query_dm(Pattern& pattern, uintptr_t vid_1, uintptr_t vid_2, std::set<std::vector<std::string>>& results_add, std::set<std::vector<std::string>>& results_del) {
        std::vector<uintptr_t> p_int;
        bool st = __pattern_to_int(pattern, p_int);
        if (st) {
            std::set<std::vector<uintptr_t>> results_int_v1;
            std::set<std::vector<uintptr_t>> results_int_v2;
            std::vector<uintptr_t> p_int2(p_int);
            p_int.push_back(vid_1);
            p_int2.push_back(vid_2);
            tuple_order query_order = get_index_dm(pattern.get_pattern());
            std::vector<uintptr_t> p_tmp = reorder_verquad(p_int, query_order);
            std::vector<uintptr_t> p_tmp2 = reorder_verquad(p_int2, query_order);
            #pragma omp parallel sections num_threads(2)
            {
                #pragma omp section
                {   
                    __trees[query_order].query(p_tmp, results_int_v1);
                }
                #pragma omp section
                {   
                    __trees[query_order].query(p_tmp2, results_int_v2);
                }                
            }
            std::set<std::vector<uintptr_t>> add_int;
            std::set<std::vector<uintptr_t>> del_int;
            __compute_delta(results_int_v1, results_int_v2, add_int, del_int);
            if (add_int.size() > 10000) {
                __unserialize_tuples_parallel(add_int, results_add, query_order);
            } else {
                __unserialize_tuples(add_int, results_add, query_order);
            }
            if (del_int.size() > 10000) {
                __unserialize_tuples_parallel(del_int, results_del, query_order);
            } else {
                __unserialize_tuples(del_int, results_del, query_order);
            }
        }        
    }

    void MemQuadVersionedDataset::__parse_and_insert(std::string str) {
        std::vector<std::vector<Term>> tuples;
        __ntparser.parse_string(str, tuples);
        for (auto& t: tuples) {
            __insert_tuple(t);
        }
    }

    void MemQuadVersionedDataset::load_from_file(std::string filename) {
        std::ifstream f(filename, std::ifstream::in);
        if (f.fail()) {
            std::cerr << "ERROR: impossible to read file \"" << filename << "\"" << std::endl;
            return;
        }
        std::cout << "Loading file \"" << filename << "\""  << std::endl;
        #pragma omp parallel num_threads(2)
        {
            #pragma omp single
            {
                uintptr_t ntriples = 0;
                std::string buffer;
                unsigned buffer_line_count = 50000;

                unsigned lc = 0;
                std::string l;
                while (std::getline(f, l)) {
                    if (l.empty()) {
                        continue;
                    }
                    buffer += l + "\n";
                    if (lc == buffer_line_count) {
                        #pragma omp task
                        {
                            __parse_and_insert(buffer);
                        }
                        buffer.clear();
                        lc = 0;
                    }
                    lc++;
                    ntriples++;
                    if (ntriples % 100000 == 0) {
                        std::cout << "\rProcessed " << ntriples << " triples..." << std::flush;
                    }
                }
                #pragma omp task
                {
                    __parse_and_insert(buffer);
                }
            }
        }
        f.close();
    }


}
