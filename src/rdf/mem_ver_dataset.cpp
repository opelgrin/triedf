// #include <omp.h>
#include <algorithm>
#include <iostream>
#include <fstream>

#include "rdf/ver_dataset.hpp"


namespace rdftrie {

    MemVersionedDataset::MemVersionedDataset(): __current_vid(1) {
        __trees.emplace(std::make_pair(SPOV, MemRDFTree2()));
        // __trees.emplace(std::make_pair(VSPO, MemRDFTree2()));
    }

    MemVersionedDataset::MemVersionedDataset(std::vector<tuple_order> add_orders): __current_vid(1) {
        __trees.emplace(std::make_pair(SPOV, MemRDFTree2()));
        // __trees.emplace(std::make_pair(VSPO, MemRDFTree2()));
        for (tuple_order to: add_orders) {
            // __trees.emplace(std::make_pair(to_vxxx(to), MemRDFTree2()));
            __trees.emplace(std::make_pair(to_xxxv(to), MemRDFTree2()));
        }
    }

    bool MemVersionedDataset::__pattern_to_int(Pattern& p, std::vector<uint64_t>& p2) {
        for (auto s: p.get_pattern()) {
            uintptr_t id = 0;
            if (s != "?") {
                Term term(s, UNKNOWN, QUERY);
                id = __dict.string_to_id(term);
                if (id == 0) {
                    return false;
                }
            }
            p2.push_back(id);          
        }
        return true;
    }

    void MemVersionedDataset::__tuple_to_string(std::vector<uintptr_t>& tuple_int, std::vector<std::string>& tuple_str) {
        for (size_t i=0; i<tuple_int.size()-1; i++) {
            tuple_str.push_back(__dict.id_to_string(tuple_int[i]));
        }
        tuple_str.push_back(std::to_string(tuple_int.back()));
    }

    void MemVersionedDataset::__unserialize_tuples(std::set<std::vector<uintptr_t>>& tuples_int, std::set<std::vector<std::string>>& tuples_str, tuple_order query_order) {
        for (auto& v: tuples_int) {
            std::vector<std::string> tuple_str;
            std::vector<uintptr_t> tmp = reorder_to_spov(v, query_order);
            __tuple_to_string(tmp, tuple_str);
            tuples_str.insert(tuple_str);
        }
    }

    void MemVersionedDataset::__unserialize_tuples_parallel(std::set<std::vector<uintptr_t>>& tuples_int, std::set<std::vector<std::string>>& tuples_str, tuple_order query_order) {
        #pragma omp parallel
        {
            #pragma omp single
            {  
                for (auto& v: tuples_int) {
                    #pragma omp task
                    {
                        std::vector<std::string> tuple_str;
                        std::vector<uint64_t> tmp = reorder_to_spov(v, query_order);
                        __tuple_to_string(tmp, tuple_str);
                        #pragma omp critical(push_results)
                        {
                            tuples_str.insert(tuple_str);
                        }
                    }
                }
            }            
        }
    }

    void MemVersionedDataset::__compute_delta(std::set<std::vector<uintptr_t>>& r1, std::set<std::vector<uintptr_t>>& r2, std::set<std::vector<uintptr_t>>& added, std::set<std::vector<uintptr_t>>& deleted) {
        std::set<std::vector<uintptr_t>> s1;
        std::set<std::vector<uintptr_t>> s2;
        #pragma omp parallel sections num_threads(2)
        {   
            #pragma omp section
            {
                for (auto& v: r1) {
                    std::vector<uintptr_t> t(v);
                    t.pop_back();
                    s1.insert(t);
                }
            }
            #pragma omp section
            {
                for (auto& v: r2) {
                    std::vector<uintptr_t> t(v);
                    t.pop_back();
                    s2.insert(t);
                }
            }            
        }
        #pragma omp parallel sections num_threads(2)
        {
            #pragma omp section
            {
                std::vector<std::vector<uint64_t>> tmp;
                std::set_difference(s1.begin(),  s1.end(), s2.begin(), s2.end(), std::back_inserter(tmp));
                deleted.insert(tmp.begin(), tmp.end());
            }
            #pragma omp section
            {
                std::vector<std::vector<uint64_t>> tmp;
                std::set_difference(s2.begin(), s2.end(), s1.begin(), s1.end(), std::back_inserter(tmp));
                added.insert(tmp.begin(), tmp.end());
            }
        }
    }

    inline tuple_order MemVersionedDataset::to_vxxx(tuple_order order) {
        switch (order) {
        case SPO:
            return VSPO;
        case SOP:
            return VSOP;
        case PSO:
            return VPSO;
        case POS:
            return VPOS;
        case OSP:
            return VOSP;
        case OPS:
            return VOPS;
        default:
            return VSPO;
        }
    }

    inline tuple_order MemVersionedDataset::to_xxxv(tuple_order order) {
        switch (order) {
        case SPO:
            return SPOV;
        case SOP:
            return SOPV;
        case PSO:
            return PSOV;
        case POS:
            return POSV;
        case OSP:
            return OSPV;
        case OPS:
            return OPSV;
        default:
            return SPOV;
        }
    }

    inline tuple_order MemVersionedDataset::get_index_dm(const std::vector<std::string>& pattern) {
        return get_index_vq(pattern);
        // if (pattern[0] == "?" && pattern[1] == "?" && pattern[2] == "?") {
		// 	return VSPO;
		// }
		// if (pattern[0] != "?") {
		// 	if (pattern[1] == "?") {
        //         if (__trees.find(VSOP) != __trees.end()) {
        //             return VSOP;
        //         } else if(__trees.find(VOPS) != __trees.end()) {
        //             return VOPS;
        //         }				
		// 	}
		// } else if (pattern[1] != "?") {
		// 	if (pattern[0] == "?") {
        //         if (__trees.find(VPOS) != __trees.end()) {
		// 		    return VPOS;
        //         } else if (__trees.find(VPSO) != __trees.end()) {
        //             return VPSO;
        //         } else if (__trees.find(VOPS) != __trees.end()) {
        //             return VOPS;
        //         }
		// 	} else {
        //         if (__trees.find(VPSO) != __trees.end()) {
		// 		    return VPSO;
        //         } else if (__trees.find(VPOS) != __trees.end()) {
		// 		    return VPOS;
		// 		}
		// 	}rdf_term_type __term_type
		// } else if (pattern[2] != "?") {
		// 	if (pattern[0] == "?") {
        //         if (__trees.find(VOPS) != __trees.end()) {
		// 		    return VOPS;
        //         } else if (__trees.find(VOSP) != __trees.end()) {
		// 		    return VOSP;
		// 		} else if (__trees.find(VPOS) != __trees.end()) {
        //             return VPOS;
        //         }
		// 	} else {
        //         if (__trees.find(VOSP) != __trees.end()) {
		// 		    return VOSP;
        //         } else if (__trees.find(VOPS) != __trees.end()) {
		// 		    return VOPS;
		// 		} else if (__trees.find(VSOP) != __trees.end()) {
        //             return VSOP;
        //         }
		// 	}
		// }
		// return VSPO;
    }

    inline tuple_order MemVersionedDataset::get_index_vm(const std::vector<std::string>& pattern) {
        return get_index_dm(pattern);
    }

    inline tuple_order MemVersionedDataset::get_index_vq(const std::vector<std::string>& pattern) {
        if (pattern[0] == "?" && pattern[1] == "?" && pattern[2] == "?") {
			return SPOV;
		}
		if (pattern[0] != "?") {
			if (pattern[1] == "?") {
                if (__trees.find(SOPV) != __trees.end()) {
                    return SOPV;
                } else if(__trees.find(OPSV) != __trees.end()) {
                    return OPSV;
                }				
			}
		} else if (pattern[1] != "?") {
			if (pattern[0] == "?") {
                if (__trees.find(POSV) != __trees.end()) {
				    return POSV;
                } else if (__trees.find(PSOV) != __trees.end()) {
                    return PSOV;
                } else if (__trees.find(OPSV) != __trees.end()) {
                    return OPSV;
                }
			} else {
                if (__trees.find(PSOV) != __trees.end()) {
				    return PSOV;
                } else if (__trees.find(POSV) != __trees.end()) {
				    return POSV;
				}
			}
		} else if (pattern[2] != "?") {
			if (pattern[0] == "?") {
                if (__trees.find(OPSV) != __trees.end()) {
				    return OPSV;
                } else if (__trees.find(OSPV) != __trees.end()) {
				    return OSPV;
				} else if (__trees.find(POSV) != __trees.end()) {
                    return POSV;
                }
			} else {
                if (__trees.find(OSPV) != __trees.end()) {
				    return OSPV;
                } else if (__trees.find(OPSV) != __trees.end()) {
				    return OPSV;
				} else if (__trees.find(SOPV) != __trees.end()) {
                    return SOPV;
                }
			}
		}
		return SPOV;
    }

    void MemVersionedDataset::__insert_tuple(std::vector<Term>& tuple, uint64_t vid) {
        // Pass strings to Dictionary and get IDs
        std::vector<uintptr_t> int_tuple;
        for (auto& t: tuple) {
            uintptr_t id = __dict.string_to_id(t);
            int_tuple.push_back(id); 
        }
        int_tuple.push_back(vid);
        for (auto& tree: __trees) {
            std::vector<uintptr_t> t = reorder_vertriple(int_tuple, tree.first);
            tree.second.put(t);
        }
        if (vid > __current_vid)
            __current_vid = vid;
    }

    void MemVersionedDataset::add_tuple(std::vector<Term>& tuple, uint64_t vid) {
        __insert_tuple(tuple, vid);
    }

    void MemVersionedDataset::query_vm(Pattern& pattern, uint64_t vid, std::set<std::vector<std::string>>& results) {
        std::set<std::vector<uintptr_t>> results_int;
        std::vector<uintptr_t> p_int;
        bool st = __pattern_to_int(pattern, p_int);
        if (st) {
            p_int.push_back(vid);
            tuple_order query_order = get_index_dm(pattern.get_pattern());
            std::vector<uintptr_t> p_tmp = reorder_vertriple(p_int, query_order);            
            __trees[query_order].query(p_tmp, results_int);
            if (results_int.size() > 10000) {
                __unserialize_tuples_parallel(results_int, results, query_order);
            } else {
                __unserialize_tuples(results_int, results, query_order);
            }
        }
    }

    void MemVersionedDataset::query(Pattern& pattern, std::set<std::vector<std::string>>& results) {
        query_vm(pattern, __current_vid, results);
    }

    void MemVersionedDataset::query_vq(Pattern& pattern, std::set<std::vector<std::string>>& results) {
        std::set<std::vector<uintptr_t>> results_int;
        std::vector<uintptr_t> p_int;
        bool st = __pattern_to_int(pattern, p_int);
        if (st) {
            p_int.push_back(0);
            tuple_order query_order = get_index_vq(pattern.get_pattern());
            std::vector<uintptr_t> p_tmp = reorder_vertriple(p_int, query_order);            
            __trees[query_order].query(p_tmp, results_int);
            if (results_int.size() > 10000) {
                __unserialize_tuples_parallel(results_int, results, query_order);
            } else {
                __unserialize_tuples(results_int, results, query_order);
            }
        }
    }

    void MemVersionedDataset::query_dm(Pattern& pattern, uint64_t vid_1, uint64_t vid_2, std::set<std::vector<std::string>>& results_add, std::set<std::vector<std::string>>& results_del) {
        std::vector<uintptr_t> p_int;
        bool st = __pattern_to_int(pattern, p_int);
        if (st) {
            std::set<std::vector<uintptr_t>> results_int_v1;
            std::set<std::vector<uintptr_t>> results_int_v2;
            std::vector<uintptr_t> p_int2(p_int);
            p_int.push_back(vid_1);
            p_int2.push_back(vid_2);
            tuple_order query_order = get_index_dm(pattern.get_pattern());
            std::vector<uintptr_t> p_tmp = reorder_vertriple(p_int, query_order);
            std::vector<uintptr_t> p_tmp2 = reorder_vertriple(p_int2, query_order);
            #pragma omp parallel sections num_threads(2)
            {
                #pragma omp section
                {   
                    __trees[query_order].query(p_tmp, results_int_v1);
                }
                #pragma omp section
                {   
                    __trees[query_order].query(p_tmp2, results_int_v2);
                }                
            }
            std::set<std::vector<uintptr_t>> add_int;
            std::set<std::vector<uintptr_t>> del_int;
            __compute_delta(results_int_v1, results_int_v2, add_int, del_int);
            if (add_int.size() > 10000) {
                __unserialize_tuples_parallel(add_int, results_add, query_order);
            } else {
                __unserialize_tuples(add_int, results_add, query_order);
            }
            if (del_int.size() > 10000) {
                __unserialize_tuples_parallel(del_int, results_del, query_order);
            } else {
                __unserialize_tuples(del_int, results_del, query_order);
            }
        }        
    }

    void MemVersionedDataset::__parse_and_insert(std::string str, uintptr_t vid) {
        std::vector<std::vector<Term>> triples;
        __ntparser.parse_string(str, triples);
        for (auto& t: triples) {
            __insert_tuple(t, vid);
        }
    }

    void MemVersionedDataset::load_from_file(std::string filename, uintptr_t vid) {
        std::ifstream f(filename, std::ifstream::in);
        if (f.fail()) {
            std::cerr << "ERROR: impossible to read file \"" << filename << "\"" << std::endl;
            return;
        }
        std::cout << "Loading file \"" << filename << "\" for dataset version " << vid << std::endl << std::endl;
        #pragma omp parallel num_threads(2)
        {
            #pragma omp single
            {
                uint64_t ntriples = 0;
                std::string buffer;
                unsigned buffer_line_count = 50000;

                unsigned lc = 0;
                std::string l;
                while (std::getline(f, l)) {
                    if (l.empty()) {
                        continue;
                    }
                    buffer += l + "\n";
                    if (lc == buffer_line_count) {
                        #pragma omp task
                        {
                            __parse_and_insert(buffer, vid);
                        }
                        buffer.clear();
                        lc = 0;
                    }
                    lc++;
                    ntriples++;
                    if (ntriples % 100000 == 0) {
                        std::cout << "\rProcessed " << ntriples << " triples..." << std::flush;
                    }
                }
                #pragma omp task
                {
                    __parse_and_insert(buffer, vid);
                }
            }
        }
        f.close();
    }


}