#include "rdf/term/rdf_term.hpp"

namespace rdftrie
{

    Term::Term(std::string str, rdf_elem_type rdf_type, rdf_term_type term_type) {
        this->__text = str;
        this->__rdf_type = rdf_type;
        this->__term_type = term_type;
    }

    rdf_elem_type Term::get_rdf_type() const {
        return this->__rdf_type;
    }

    rdf_term_type Term::get_term_type() const {
        return this->__term_type;
    }

    std::string Term::get_string() const {
        return this->__text;
    }
    
    bool Term::operator==(const Term& other) const {
        return other.get_string() == this->__text 
            && other.get_rdf_type() == this->__rdf_type
            && other.get_term_type() == this->__term_type;
    }

    bool Term::operator!=(const Term& other) const {
        return !(*this == other);
    }

    size_t Term::get_hash() const {
        size_t str_hash = std::hash<std::string>()(this->__text);
        size_t tt_hash = size_t(this->__term_type);
        size_t rt_hash = size_t(this->__rdf_type);
        
        return ((str_hash ^ tt_hash << 1) >> 1) ^ (rt_hash << 1);
    }

}