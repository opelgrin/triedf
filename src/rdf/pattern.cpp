#include "rdf/pattern.hpp"

namespace rdftrie {

    Pattern::Pattern(std::vector<std::string> pattern, tuple_order pref_order) {
        __pattern = pattern;
        __pref_order = pref_order;
    }

    const std::vector<std::string>& Pattern::get_pattern() {
        return __pattern;
    }

    tuple_order Pattern::get_preferred_order() {
        return __pref_order;
    }

}