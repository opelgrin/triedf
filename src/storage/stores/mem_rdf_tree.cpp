#include "storage/stores/mem_rdf_tree.hpp"


namespace rdftrie
{

    MemRDFTree::MemRDFTree(): __node_id(2) { // Node 0 dont exist, Node 1 is the root, so start at 2
        uint64_t root_id = 1; 
        this->__nodes.emplace(std::make_pair(root_id, MemNode()));
    }

    MemRDFTree::MemRDFTree(const uint64_t start_node_id) {
        this->__node_id = start_node_id;
    }

    uint64_t MemRDFTree::__get_new_nodeid() {
        uint64_t tmp = this->__node_id;
        this->__node_id++;
        return tmp;
    }

    void MemRDFTree::put(std::vector<uint64_t> values) {
        uint64_t current_node_id = 1;
        for (uint64_t val: values) {
            uint64_t child_id = this->__nodes[current_node_id].get_child(val);
            if (child_id != 0) { // Node already exist, we move forward
                current_node_id = child_id;
            } else { // Node don't exist, we add
                uint64_t new_node_id = this->__get_new_nodeid();
                this->__nodes.emplace(std::make_pair(new_node_id, MemNode()));
                this->__nodes[current_node_id].add_child(val, new_node_id);
                current_node_id = new_node_id;
            }
        }
    }

    void MemRDFTree::get(std::vector<uint64_t> values, std::set<std::vector<uint64_t>>& tuples) {
        uint64_t current_node_id = 1;
        for (uint64_t val: values) {
            uint64_t child_id = this->__nodes[current_node_id].get_child(val);
            if (child_id != 0) { // Node already exist, we move forward
                current_node_id = child_id;
            } else {
                return;
            }
        }
        std::set<std::vector<uint64_t>> vec;
        std::vector<uint64_t> path;
        this->__get_node_children_rec(current_node_id, path, vec);
        for (auto children: vec) {
            std::vector<uint64_t> r(values);
            r.insert(r.end(), children.begin(), children.end());
            tuples.insert(r);
        }
    }

    void MemRDFTree::__get_node_children_rec(uint64_t node_id, std::vector<uint64_t> path, std::set<std::vector<uint64_t>>& results) {
        auto children = this->__nodes[node_id].get_children();
        if (children.size() == 0) {
            results.insert(path);
        } else {
            for (auto child: children) {
                uint64_t node_key = child.first;
                std::vector<uint64_t> new_path(path);
                new_path.push_back(node_key);
                this->__get_node_children_rec(child.second, new_path, results);
            }
        }
    }

    bool MemRDFTree::exist(std::vector<uint64_t> values) {
        uint64_t current_node_id = 1;
        for (uint64_t val: values) {
            uint64_t child_id = this->__nodes[current_node_id].get_child(val);
            if (child_id != 0) { // Node already exist, we move forward
                current_node_id = child_id;
            } else {
                return false;
            }
        }
        return true;
    }

    void MemRDFTree::__rec_count(uint64_t node_id, uint64_t& count) {
        auto children = this->__nodes[node_id].get_children();
        if (children.size() == 0) {
            count++;
        } else {
            for (auto child: children) {
                this->__rec_count(child.second, count);
            }
        }
    }

    uint64_t MemRDFTree::count_tuples() {
        uint64_t count = 0;
        this->__rec_count(1UL, count);
        return count;
    }

    void MemRDFTree::__rec_query(std::vector<uint64_t> query, unsigned query_ind, uint64_t node_id, std::vector<uint64_t> path, std::set<std::vector<uint64_t>>& results) {
        if (query_ind == query.size()) {
            results.insert(path);
        } else {
            if (query[query_ind] == 0) {
                auto children = this->__nodes[node_id].get_children();
                for (auto c: children) {
                    std::vector<uint64_t> new_path(path);
                    new_path.push_back(c.first);
                    this->__rec_query(query, query_ind+1, c.second, new_path, results);
                }
            } else {
                uint64_t child = this->__nodes[node_id].get_child(query[query_ind]);
                std::vector<uint64_t> new_path(path);
                new_path.push_back(query[query_ind]);
                this->__rec_query(query, query_ind+1, child, new_path, results);
            }
        }
    }

    void MemRDFTree::query(const std::vector<uint64_t>& pattern, std::set<std::vector<uint64_t>>& tuples) {
        std::vector<uint64_t> path;
        this->__rec_query(pattern, 0, 1, path, tuples);
    }


}
