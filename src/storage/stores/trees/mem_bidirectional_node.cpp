#include "storage/stores/trees/mem_bidirectional_node.hpp"

namespace rdftrie
{

    MemBidirectionalNode::MemBidirectionalNode(): __value(0), __parent_id(0), __children(nullptr) {}

    MemBidirectionalNode::MemBidirectionalNode(const char* key, uint64_t parent): __value(0), __children(nullptr) {
        this->__parent_id = parent;
        this->__key = key;
    }

    MemBidirectionalNode::MemBidirectionalNode(const char* key, uint64_t value, uint64_t parent): __children(nullptr) {
        this->__value = value;
        this->__parent_id = parent;
        this->__key = key;
    }

    MemBidirectionalNode::MemBidirectionalNode(uint64_t parent): __value(0), __children(nullptr), __key(nullptr) {
        this->__parent_id = parent;
    }

    uint64_t MemBidirectionalNode::get_child(std::string key) {
        if (this->__children) {
            return this->__children->get(key);
        } else {
            return 0UL;
        }        
    }

    const char* MemBidirectionalNode::add_child(const std::string key, uint64_t node_id) {
        if (!this->__children) {
            this->__children = new MemChildren<std::string>;
        }
        return this->__children->set(key, node_id)->c_str();
    }

    std::vector<std::pair<std::string, uint64_t>> MemBidirectionalNode::get_children() {
        if (this->__children) {
            return this->__children->list();
        } else {
            std::vector<std::pair<std::string, uint64_t>> c;
            return c;
        }
    }

    uint64_t MemBidirectionalNode::get_value() {
        return this->__value;
    }
    
    void MemBidirectionalNode::set_value(uint64_t value) {
        this->__value = value;
    }

    void MemBidirectionalNode::set_key(const char* key) {
        this->__key = key;
    }

    void MemBidirectionalNode::set_parent(uint64_t parent_id) {
        this->__parent_id = parent_id;
    }

    uint64_t MemBidirectionalNode::get_parent() {
        return this->__parent_id;
    }

    std::string MemBidirectionalNode::get_key() {
        return std::string(this->__key);
    }

    MemBidirectionalNode::~MemBidirectionalNode() {
        delete __children;
    }

    // --------------------------------------------------------------    

    MemBidirectionalNode2::MemBidirectionalNode2(): __value(0), __parent(nullptr), __children(nullptr), __key(nullptr) {}

    MemBidirectionalNode2::MemBidirectionalNode2(const std::string* key, const MemBidirectionalNode2* parent): __value(0), __children(nullptr) {
        __key = key;
        __parent = parent;
    }

    MemBidirectionalNode2::MemBidirectionalNode2(const std::string* key, uint64_t value, const MemBidirectionalNode2* parent): __children(nullptr) {
        __key = key;
        __value = value;
        __parent = parent;
    }

    MemBidirectionalNode2::MemBidirectionalNode2(const MemBidirectionalNode2* parent): __value(0), __children(nullptr), __key(nullptr) {
        __parent = parent;
    }

    MemBidirectionalNode2* MemBidirectionalNode2::get_child(std::string& key) const
    {
        if (__children != nullptr)
        {
            auto r = __children->find(key);
            if (r != __children->end())
                return &((*r).second);
        }
        return nullptr;
    }

    MemBidirectionalNode2* MemBidirectionalNode2::make_child(std::string key)
    {
        if(__children == nullptr) {
            __children = new std::map<std::string, MemBidirectionalNode2>;
        }
        auto ret = __children->emplace(std::piecewise_construct,  std::forward_as_tuple(key), std::forward_as_tuple(this));
        ret.first->second.set_key(&(ret.first->first));
        return &(ret.first->second);
    }

    std::string MemBidirectionalNode2::get_key() const {
        if (!__key) {
            return std::string();
        }
        return std::string(__key->c_str());
    }

    uint64_t MemBidirectionalNode2::get_value() const {
        return __value;
    }

    const MemBidirectionalNode2* MemBidirectionalNode2::get_parent() const {
        return __parent;
    }

    void MemBidirectionalNode2::set_value(uint64_t value) {
        __value = value;
    }

    void MemBidirectionalNode2::set_parent(const MemBidirectionalNode2* parent) {
        __parent = parent;
    }

    void MemBidirectionalNode2::set_key(const std::string* key) {
        __key = key;
    }

    MemBidirectionalNode2::~MemBidirectionalNode2()
    {
        delete __children;
    }

}