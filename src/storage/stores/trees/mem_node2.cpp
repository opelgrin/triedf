#include "storage/stores/trees/mem_node2.hpp"

namespace rdftrie {
    
    std::pair<bool, MemNode2*> MemNode2::get_child(uintptr_t key) const {
        if (__children) {
            auto r =  __children->find(key);
            if (r != __children->end()) {
                return std::pair<bool, MemNode2*>(true, &(r->second));
            }
        }
        return std::pair<bool, MemNode2*>(false, nullptr);
    }

    MemNode2* MemNode2::make_child(uintptr_t key, bool make_leaf) {
        if (!__children) {
            __children = std::unique_ptr<std::map<uintptr_t, MemNode2>>(new std::map<uint64_t, MemNode2>);
        }
        auto ret = __children->emplace(std::make_pair(key, MemNode2()));
        return &(ret.first->second);
    }

    bool MemNode2::has_children() const {
        if (__children) {
            if (__children->size() > 0)
                return true;
        }
        return false;
    }

    std::map<uintptr_t, MemNode2>::iterator MemNode2::begin() {
        if (!__children) {
            __children = std::unique_ptr<std::map<uintptr_t, MemNode2>>(new std::map<uint64_t, MemNode2>);
        }
        return __children->begin();
    }

    std::map<uintptr_t,MemNode2>::iterator MemNode2::end() {
        if (!__children) {
            __children = std::unique_ptr<std::map<uintptr_t, MemNode2>>(new std::map<uint64_t, MemNode2>);
        }
        return __children->end();
    }       


}