#include <iostream>

#include "storage/stores/trees/mem_node.hpp"

namespace rdftrie 
{

    MemNode::MemNode(): __children(nullptr) {}

    uint64_t MemNode::get_child(uint64_t key) {
        if (__children) {
            return __children->get(key);
        } else {
            return 0;
        }
    }

    void MemNode::add_child(const uint64_t key, uint64_t node_id) {
        if (!__children) {
            __children = new MemChildren<uint64_t>;
        }       
        __children->set(key, node_id);
    }

    std::vector<std::pair<uint64_t, uint64_t>> MemNode::get_children() {
        if (__children) {
            return __children->list();
        } else {
            std::vector<std::pair<uint64_t, uint64_t>> ret;
            return ret;
        }
    }

    uint64_t MemNode::get_value() {
        return 0;
    }
    
    uint64_t MemNode::num_children() {
        return __children->list().size();
    }

    MemNode::~MemNode() {
        delete __children;
    }


}