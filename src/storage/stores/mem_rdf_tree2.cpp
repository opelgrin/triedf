#include <iostream>

#include "storage/stores/mem_rdf_tree2.hpp"


namespace rdftrie {

    MemRDFTree2::MemRDFTree2() {
        __mutex = std::unique_ptr<std::shared_mutex>(new std::shared_mutex);
    }

    void MemRDFTree2::__get_node_children_rec(MemNode2* node, std::vector<uintptr_t> path, std::set<std::vector<uintptr_t>>& results) {
        if (!node->has_children()) {
            results.insert(path);
        } else {
            auto it = node->begin();
            while(it != node->end()) {
                uintptr_t node_key = it->first;
                std::vector<uintptr_t> new_path(path);
                new_path.push_back(node_key);
                __get_node_children_rec(&(it->second), new_path, results);
                it++;
            }
        }
    }

    void MemRDFTree2::__rec_count(MemNode2* node, uint64_t& count) {
        if (!node->has_children()) {
            count++;
        } else {
            auto it = node->begin();
            while(it != node->end()) {
                this->__rec_count(&(it->second), count);
                it++;
            }
        }
    }

    void MemRDFTree2::__rec_query(std::vector<uintptr_t> query, unsigned query_ind, MemNode2* node, std::vector<uintptr_t> path, std::set<std::vector<uintptr_t>>& results) {
        // if (query_ind == query.size()) {
        if (!node->has_children()) {
            results.insert(path);
        } else {
            if (query[query_ind] == 0) {
                if (node->has_children()) {
                    auto it = node->begin();
                    while(it != node->end()) {
                        std::vector<uint64_t> new_path(path);
                        new_path.push_back(it->first);
                        __rec_query(query, query_ind+1, &(it->second), new_path, results);
                        it++;
                    }
                }
            } else {
                std::pair<bool, MemNode2*> child = node->get_child(query[query_ind]);
                if (child.first) {
                    std::vector<uint64_t> new_path(path);
                    new_path.push_back(query[query_ind]);
                    this->__rec_query(query, query_ind+1, child.second, new_path, results);
                }                
            }
        }
    }

    void MemRDFTree2::put(std::vector<uintptr_t> values) {
        std::unique_lock ul(*__mutex);
        MemNode2* current_node = &__root;
        for (size_t i=0; i<values.size(); i++) {
            std::pair<bool, MemNode2*> child = current_node->get_child(values[i]);
            if (child.second) { // Node already exist, we move forward
                current_node = child.second;
            } else { // Node don't exist, we add
                bool make_leaf = (i == values.size()-1);
                MemNode2* new_node = current_node->make_child(values[i], make_leaf);
                current_node = new_node;
            }
        }
    }

    void MemRDFTree2::get(std::vector<uintptr_t> values, std::set<std::vector<uintptr_t>>& tuples) {
        std::shared_lock sl(*__mutex);
        MemNode2* current_node = &__root;
        for (uintptr_t val: values) {
            std::pair<bool, MemNode2*> child_node = current_node->get_child(val);
            if (child_node.second) { // Node already exist, we move forward
                current_node = child_node.second;
            } else {
                return;
            }
        }
        std::set<std::vector<uintptr_t>> vec;
        std::vector<uintptr_t> path;
        __get_node_children_rec(current_node, path, vec);
        for (auto& children: vec) {
            std::vector<uintptr_t> r(values);
            r.insert(r.end(), children.begin(), children.end());
            tuples.insert(r);
        }
    }

    bool MemRDFTree2::exist(std::vector<uintptr_t> values) {
        std::shared_lock sl(*__mutex);
        MemNode2* current_node = &__root;
        for (uintptr_t val: values) {
            std::pair<bool, MemNode2*> child_node = current_node->get_child(val);
            if (child_node.second) { // Node already exist, we move forward
                current_node = child_node.second;
            } else {
                return false;
            }
        }
        return true;
    }

    uint64_t MemRDFTree2::count_tuples() {
        std::shared_lock sl(*__mutex);
        uint64_t count = 0;
        __rec_count(&__root, count);
        return count;
    }

    void MemRDFTree2::query(const std::vector<uintptr_t>& pattern, std::set<std::vector<uintptr_t>>& tuples) {
        std::shared_lock sl(*__mutex);
        std::vector<uintptr_t> path;
        __rec_query(pattern, 0, &__root, path, tuples);
    }

}