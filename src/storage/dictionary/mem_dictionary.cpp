#include "storage/dictionary/mem_dictionary.hpp"

namespace rdftrie 
{

    MemDictionary::MemDictionary(): __current_id(1) {}

    MemDictionary::MemDictionary(const uint64_t start_id) {
        this->__current_id = start_id;
    }

    uint64_t MemDictionary::string_to_id(const Term term) {
        uint64_t id = this->__current_id;
        if (term.get_rdf_type() == UNKNOWN) {
            id = 0;
            id = this->__uri_dict.string_to_id(term.get_string(), id, true);
            if (id == 0)
                id = this->__oth_dict.string_to_id(term.get_string(), id, true);
        } else {
            if (term.get_rdf_type() == URI) {
                id = this->__uri_dict.string_to_id(term.get_string(), id);
            } else {
                id = this->__oth_dict.string_to_id(term.get_string(), id);
            }
            if (this->__current_id == id) {
                this->__current_id++;
            }
        }        
        return id;
    }

    std::string MemDictionary::id_to_string(const uint64_t id) {
        std::string ret;
        ret = this->__uri_dict.id_to_string(id);
        if (ret == "") {
            ret = this->__oth_dict.id_to_string(id);
        }
        return ret;
    }



    MapMemDictionary::MapMemDictionary(): __current_id(1) {}

    MapMemDictionary::MapMemDictionary(uint64_t start_id) {
        __current_id = start_id;
    }

    uint64_t MapMemDictionary::string_to_id(const Term term) {
        uint64_t id = __current_id;
        if (term.get_rdf_type() == UNKNOWN) {
            id = 0;
            id = __dict.string_to_id(term.get_string(), id, true);
        } else {
            id = __dict.string_to_id(term.get_string(), id);
            if (__current_id == id) {
                __current_id++;
            }
        }        
        return id;
    }

    std::string MapMemDictionary::id_to_string(const uint64_t id) {
        std::string ret;
        ret = this->__dict.id_to_string(id);
        return ret;
    }

}