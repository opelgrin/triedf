#include <algorithm>

#include "storage/dictionary/mem_dict_trie.hpp"
#include "util/util.hpp"

namespace rdftrie 
{

    MemDictTrie::MemDictTrie(): __node_id(2) {
        uint64_t root_id = 1;
        this->__nodes.emplace(std::make_pair(root_id, MemBidirectionalNode()));
    }

    uint64_t MemDictTrie::__get_new_nodeid() {
        uint64_t tmp = this->__node_id;
        this->__node_id++;
        return tmp;
    }

    uint64_t MemDictTrie::__set(const std::vector<std::string> elements, uint64_t id, bool no_insert) {
        uint64_t current_node_id = 1;
        for (std::string val: elements) {
            uint64_t child_id = this->__nodes[current_node_id].get_child(val);
            if (child_id != 0) { // Node already exist, we move forward
                current_node_id = child_id;
            } else if (!no_insert) { // Node don't exist, we add
                uint64_t new_node_id = this->__get_new_nodeid();
                this->__nodes.emplace(std::piecewise_construct, std::forward_as_tuple(new_node_id), std::forward_as_tuple(current_node_id));
                const char* str_p = this->__nodes[current_node_id].add_child(val, new_node_id);
                this->__nodes[new_node_id].set_key(str_p);
                current_node_id = new_node_id;
            } else { // no_insert == true
                return 0;
            }
        }
        uint64_t str_id = this->__nodes[current_node_id].get_value();
        if (str_id == 0 && !no_insert) {
            str_id = id;
            this->__nodes[current_node_id].set_value(str_id);
            this->__rev_trie.insert(std::make_pair(str_id, current_node_id));
        }
        return str_id;
    }

    std::vector<std::string> MemDictTrie::__get(const uint64_t id) {
        std::vector<std::string> ret;
        if (this->__rev_trie.find(id) == this->__rev_trie.end())
            return ret;
        uint64_t target_node = this->__rev_trie[id];
        ret.push_back(this->__nodes[target_node].get_key());
        uint64_t parent_id = this->__nodes[target_node].get_parent();
        while(parent_id != 1) {
            ret.push_back(this->__nodes[parent_id].get_key());
            parent_id = this->__nodes[parent_id].get_parent();
        }
        std::reverse(ret.begin(), ret.end());
        return ret;
    }

    uint64_t MemDictTrie::string_to_id(std::string str, uint64_t id, bool no_inserts) {
        std::vector<std::string> parts;
        split(str, "/", parts);
        return this->__set(parts, id);
    }

    std::string MemDictTrie::id_to_string(uint64_t id) {
        std::vector<std::string> parts = this->__get(id);
        if (parts.size() > 0) {
            std::string ret = parts[0];
            for (size_t i=1; i<parts.size(); i++) {
                ret += "/";
                ret += std::string(parts[i]);
            }
            return ret;
        }
        return "";
    }


    // ----------------------------------------------

    MemDictTrie2::MemDictTrie2() {
        __mutex = std::unique_ptr<std::shared_mutex>(new std::shared_mutex);
    }

    uint64_t MemDictTrie2::__set(const std::vector<std::string> elements, uint64_t id, bool no_insert)
    {
        std::unique_lock ul(*__mutex);
        MemBidirectionalNode2* current_node = &__root;
        for (std::string val: elements) {
            MemBidirectionalNode2* child_node = current_node->get_child(val);
            if (child_node) { // Node already exist, we move forward
                current_node = child_node;
            } else if (!no_insert) { // Node don't exist, we add
                MemBidirectionalNode2* new_node = current_node->make_child(val);
                current_node = new_node;
            } else { // no_insert == true
                return 0;
            }
        }
        uint64_t str_id = current_node->get_value();
        if (str_id == 0 && !no_insert) {
            str_id = id;            
            current_node->set_value(str_id);
            __rev_trie.insert(std::make_pair(str_id, current_node));
        }
        return str_id;
    }

    std::vector<std::string> MemDictTrie2::__get(uint64_t id) {
        std::shared_lock sl(*__mutex);
        std::vector<std::string> ret;
        if (__rev_trie.find(id) == __rev_trie.end())
            return ret;
        MemBidirectionalNode2* target_node = __rev_trie[id];
        ret.push_back(target_node->get_key());
        const MemBidirectionalNode2* parent = target_node->get_parent();
        while(parent != nullptr) {
            std::string k = parent->get_key();
            if (!k.empty())
                ret.push_back(k);
            parent = parent->get_parent();
        }
        std::reverse(ret.begin(), ret.end());
        return ret;
    }

    uint64_t MemDictTrie2::string_to_id(std::string str, uint64_t id, bool no_insert) {
        std::vector<std::string> parts;
        split(str, "/", parts);
        return __set(parts, id);
    }
 
    std::string MemDictTrie2::id_to_string(uint64_t id) {
        std::vector<std::string> parts = __get(id);
        if (parts.size() > 0) {
            std::string ret = parts[0];
            for (size_t i=1; i<parts.size(); i++) {
                ret += "/";
                ret += std::string(parts[i]);
            }
            return ret;
        }
        return "";
    }
    
}