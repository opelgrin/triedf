#include "storage/dictionary/dict_map.hpp"

namespace rdftrie {

    DictMap::DictMap() {
        __mutex = std::unique_ptr<std::shared_mutex>(new std::shared_mutex);
    }

    uint64_t DictMap::string_to_id(std::string str, uint64_t id, bool no_insert) {
        std::shared_lock sl(*__mutex);
        if (this->__str_id.find(str) == this->__str_id.end()) {
            if (no_insert)
                return 0;
            sl.unlock();
            std::unique_lock ul(*__mutex);
            this->__str_id.insert(std::make_pair(str, id));
            this->__id_str.insert(std::make_pair(id, str.c_str()));
            return id;
        } else {
            return this->__str_id[str];
        }
    }

    std::string DictMap::id_to_string(uint64_t id) {
        std::shared_lock sl(*__mutex);
        if (this->__id_str.find(id) != this->__id_str.end()) {
            return this->__id_str[id];
        }
        return "";
    }

}