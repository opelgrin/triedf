#include <algorithm>

#include "storage/dictionary/trie_dictionary.hpp"
#include "util/util.hpp"

namespace rdftrie {

    MemLeafNode::MemLeafNode(): __key(nullptr) {}

    TrieNode* MemLeafNode::get_child(std::string& key) const {
        return nullptr;
    }

    TrieNode* MemLeafNode::make_child(std::string key, bool make_leaf) {
        return nullptr;
    }

    std::string MemLeafNode::get_key() const {
        if (__key)
            return std::string(__key->c_str());
        return "";
    }

    void MemLeafNode::set_key(const std::string* key) {
        __key = key;
    }

    const TrieNode* MemLeafNode::get_parent() const {
        return nullptr;
    }

    void MemLeafNode::set_parent(const TrieNode* parent) {}


    // ------------------------------------------------
    MemTrieNode::MemTrieNode(): __parent(nullptr), __key(nullptr), __children(nullptr) {}

    TrieNode* MemTrieNode::get_child(std::string& key) const {
        if (__children) {
            auto r = __children->find(key);
            if (r != __children->end())
                return &(r->second);
        }
        return nullptr;
    }

    TrieNode* MemTrieNode::make_child(std::string key, bool make_leaf) {
        if(!__children) {
            __children = new std::map<std::string, MemTrieNode>;
        }
        auto ret = __children->emplace(std::make_pair(key, MemTrieNode()));
        ret.first->second.set_key(&(ret.first->first));
        ret.first->second.set_parent(this);
        return &(ret.first->second);
    }

    std::string MemTrieNode::get_key() const {
        if (__key)
            return std::string(__key->c_str());
        return "";
    }

    void MemTrieNode::set_key(const std::string* key) {
        __key = key;
    }

    const TrieNode* MemTrieNode::get_parent() const {
        return __parent;
    }

    void MemTrieNode::set_parent(const TrieNode* parent) {
        __parent = (MemTrieNode*) parent;
    }

    MemTrieNode::~MemTrieNode() {
        delete __children;
    }
    
    // ---------------------------------------------------------

    TrieNode* MemRootNode::get_child(std::string& key) const {
        auto r = __children.find(key);
        if (r != __children.end())
            return (TrieNode*) &(r->second);
        auto r2 = __children_leaf.find(key);
        if (r2 != __children_leaf.end())
            return (TrieNode*) &(r2->second);
        return nullptr;
    }

    TrieNode* MemRootNode::make_child(std::string key, bool make_leaf) {
        if (make_leaf) {
            auto ret = __children_leaf.emplace(std::make_pair(key, MemLeafNode()));
            ret.first->second.set_key(&(ret.first->first));
            return &(ret.first->second);
        } else {
            auto ret = __children.emplace(std::make_pair(key, MemTrieNode()));
            ret.first->second.set_key(&(ret.first->first));
            ret.first->second.set_parent(this);
            return &(ret.first->second);
        }
        return nullptr;
    }

    std::string MemRootNode::get_key() const {
        return "";
    }

    void MemRootNode::set_key(const std::string* key) {}

    const TrieNode* MemRootNode::get_parent() const {
        return nullptr;
    }

    void MemRootNode::set_parent(const TrieNode* parent) {}

    // ---------------------------------------------------------

    MemTrieDictionary::MemTrieDictionary() {
        __mutex = std::unique_ptr<std::shared_mutex>(new std::shared_mutex);
    }

    uintptr_t MemTrieDictionary::__set_uri(const std::vector<std::string>& elements, bool no_insert) {
        TrieNode* current_node = (TrieNode*) &__root;
        for (std::string val: elements) {
            TrieNode* child_node = current_node->get_child(val);
            if (child_node) {
                current_node = (MemTrieNode*) child_node;
            } else if (!no_insert) {
                MemTrieNode* new_node = (MemTrieNode*) current_node->make_child(val);
                current_node = new_node;
            } else {
                return 0;
            }
        }
        return (uintptr_t) current_node;
    }

    uintptr_t MemTrieDictionary::__set_other(std::string element, bool no_insert) {
        uintptr_t tmpid = 0;
        MemLeafNode* current_node = (MemLeafNode*) __root.get_child(element);
        if (!current_node && !no_insert) {
            current_node = (MemLeafNode*) __root.make_child(element, true);
        }
        if (current_node)
            tmpid = (uintptr_t) current_node;
        return tmpid;
    }

    void MemTrieDictionary::__get(uintptr_t id, std::vector<std::string>& ret) {
        std::shared_lock sl(*__mutex);
        TrieNode* target_node = (TrieNode*) id;
        ret.push_back(target_node->get_key());
        const TrieNode* parent = target_node->get_parent();
        while(parent) {
            std::string k = parent->get_key();
            if (!k.empty())
                ret.push_back(k);
            parent = parent->get_parent();
        }
        std::reverse(ret.begin(), ret.end());
    }

    uintptr_t MemTrieDictionary::string_to_id(const Term term) {
        if (term.get_rdf_type() == UNKNOWN) {
            std::shared_lock sl(*__mutex);
            uintptr_t id = __set_other(term.get_string(), true);
            if (id == 0) {
                std::vector<std::string> parts;
                split(term.get_string(), "/", parts);
                id = __set_uri(parts, true); 
            }
            return id;
        } else if (term.get_rdf_type() == URI) {
            std::vector<std::string> parts;
            split(term.get_string(), "/", parts);
            std::unique_lock ul(*__mutex);
            return __set_uri(parts);
        } else {
            std::unique_lock ul(*__mutex);
            return __set_other(term.get_string());
        }
    }

    std::string MemTrieDictionary::id_to_string(uintptr_t id) {
        if (id != 0) {
            std::vector<std::string> parts;
            __get(id, parts);
            if (parts.size() > 1) {
                std::string ret = parts[0];
                for (size_t i=1; i<parts.size(); i++) {
                    ret += "/";
                    ret += std::string(parts[i]);
                }
                return ret;
            } else if (parts.size() > 0) {
                return parts[0];
            }
        }
        return "";
    }


}