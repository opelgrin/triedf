#include <cstring>

#include "storage/bin_data.hpp"

namespace rdftrie {

    BinData::BinData(): __size(0), __data(nullptr) {}

    BinData::BinData(const std::string& data) {
        this->__size = data.length() + 1;
        this->__data = new char[this->__size];
        std::memcpy(this->__data, data.c_str(), this->__size);
    }

    BinData::BinData(const std::vector<char>& data) {
        this->__size = data.size();
        this->__data = new char[this->__size];
        std::memcpy(this->__data, data.data(), this->__size);
    }

    BinData::BinData(const char* data, size_t size) {
        this->__size = size;
        this->__data = new char[this->__size];
        std::memcpy(this->__data, data, this->__size);
    }

    BinData::BinData(const uint64_t data) {
        this->__size = sizeof(uint64_t);
        this->__data = new char[this->__size];
        std::memcpy(this->__data, (char*)&data, this->__size);
    }

    BinData::BinData(const BinData& other): __data(nullptr) {
        this->__size = other.get_size();
        if (this->__size != 0) {
            this->__data = new char[this->__size];
            std::memcpy(this->__data, other.get_data(), this->__size);
        }
    }

    BinData::~BinData() {
        delete [] this->__data;
    }

    const char* BinData::get_data() const {
        return this->__data;
    }

    void BinData::set_data(char* data, size_t size) {
        if (this->__data)
            delete [] this->__data;
        this->__data = new char[size];
        std::memcpy(this->__data, data, size);
        this->__size = size;
    }

    const size_t BinData::get_size() const {
        return this->__size;
    }

    bool BinData::operator==(const BinData& other) const {
        bool equal = false;
        if (this->__size == other.get_size()) {
            if (std::memcmp(this->__data, other.get_data(), this->__size) == 0) {
                equal = true;
            }
        }
        return equal;
    }

    bool BinData::operator!=(const BinData& other) const {
        return !(*this == other);
    }

    bool BinData::operator<(const BinData& other) const {
        bool inf = false;
        if (this->__size == other.get_size()) {
            int comp = std::memcmp(this->__data, other.get_data(), this->__size);
            if (comp < 0) {
                inf = true;
            }
        } else if (this->__size < other.get_size()) {
            inf = true;
        }
        return inf;
    }

    uint64_t BinData::to_uint64() {
        uint64_t ret = 0;
        std::memcpy(&ret, this->__data, sizeof(uint64_t));
        return ret;
    }

    // sdbm hash (is it actually the best method ?)
    size_t BinData::get_hash() const {
        size_t hash = 0;
        int c;

        while ((c = (*this->__data)++))
            hash = c + (hash << 6) + (hash << 16) - hash;

        return hash;
    }

}